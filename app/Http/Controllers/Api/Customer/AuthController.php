<?php

namespace App\Http\Controllers\Api\Customer;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\UserResource;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Traits\GeneralHelperTrait;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;


class AuthController extends Controller
{
    use GeneralHelperTrait;

    public $successStatus = 200;

    public function register(Request $request)
    {
        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'date_of_birth' => 'required',
            'nationality' => 'required',
            'occupation' => 'required',
            'institution_name' => 'required',
            'address' => 'required',
            'address_two' => 'required',
            'region_capital' => 'required',
            'country' => 'required',
            'phone_number' => 'required|unique:users,phone_number',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'date_of_birth.required' => 'City is required.',
            'nationality.required' => 'Area is required.',
            'occupation.required' => 'Gender is required.',
            'institution_name.required' => 'Age is required.',
            'address.required' => 'Phone Number is required.',
            'address_two.required' => 'CNIC is required.',
            'region_capital.required' => 'Address is required.',
            'country.required' => 'Address is required.',
            'phone_number.required' => 'Address is required.',
            'email.required' => 'Email is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        $role = Role::find(2);

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'date_of_birth' => $request->get('date_of_birth'),
            'nationality' => $request->get('nationality'),
            'role_id' => $role->id,
            'occupation' => $request->get('occupation'),
            'institution_name' => $request->get('institution_name'),
            'address' => $request->get('address'),
            'address_two' => $request->get('address_two'),
            'region_capital' => $request->get('region_capital'),
            'country' => $request->get('country'),
            'phone_number' => $request->get('phone_number'),
            'referral_code' => $random_string,
            'user_type' => $role->name,
            'email' => $request->get('email'),
            'fcm_token' => $request->get('fcm_token'),
            'password' => bcrypt($request->get('password')),
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $phone_number = $request->get('phone_number');
        $user = $this->sentOtpCode($phone_number);

        $data = [
            'phone_number' => $phone_number,
            'code' => $user->otp_code
        ];
        $message = 'Thank you for becoming a member of Saver Buddy! Your OTP Code is ' . $user->otp_code . ' Please enter this code to verify your mobile number.';
        $smsResponse = $this->sendSMS($phone_number, $message);

        return response()->json(['success' => true, 'message' => $message, 'data' => $data, 'is_otp_send' => true], 200);
    }

    public function login(Request $request)
    {
        $user = User::where([
            'phone_number' => $request->phone_number,
            'user_type' => 'customer'
        ])->first();

        if (!is_null($user)) {
            $validator = Validator::make($request->all(), [
                'phone_number' => 'required|exists:users,phone_number',
                'password' => 'required'
            ], [
                'phone_number.required' => 'Phone Number is required',
                'password.required' => 'Password is required',

            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
            }

            if (Auth::attempt(['phone_number' => request('phone_number'), 'password' => request('password')])) {
                $user = Auth::user();

                if ($user->status == 'suspended') {
                    return response()->json(['success' => false, 'message' => 'Your account is suspended.', 'data' => [], 'is_otp_send' => false], 200);
                }
                if ($user->otp_status !== 'verified') {
                    $user = $this->sentOtpCode($user->phone_number);

                    $data = [
                        'phone_number' => $user->phone_number,
                        'code' => $user->otp_code
                    ];
                    // event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));

                    $message = 'Your account is not verfied. Your OTP Code is ' . $user->otp_code . ' Please enter this code to verify your mobile number.';
                    $smsResponse = $this->sendSMS($user->phone_number, $message);
                    return response()->json(['success' => true, 'message' => $message, 'data' => $data, 'is_otp_send' => true], 200);
                }

                $success['token'] = $user->createToken('AppName')->accessToken;
                $success['user'] = new UserResource($user);
                return response()->json(['success' => true, 'message' => 'User is found', 'data' => $success, 'is_otp_send' => false], 200);
            } else {
                return response()->json(['success' => true, 'message' => 'Invalid Password', 'data' => [], 'is_otp_send' => false], 200);
            }

        } else {
            $message = 'User not found';
            return response()->json(['success' => false, 'message' => $message, 'data' => [], 'is_otp_send' => false], 200);

        }

    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|exists:users,phone_number',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        try {
            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'otp_code' => $request->get('otp_code'),
                'user_type' => 'customer'
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->status = 'verified';
                $user->email_verified_at = now();
                $user->otp_code = null;
                $user->save();

                return response()->json(['success' => true, 'message' => 'OTP Code verified successfully.', 'data' => []], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'Provided OTP code is not correct.', 'data' => []], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function logout()
    {


        try {
            $user = Auth::user();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }
            return response()->json(['success' => true, 'message' => '', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $exception], 200);
        }
    }


    public function getUser()
    {
        $user = new UserResource(Auth::user());
        return response()->json(['success' => true, 'message' => '', 'data' => $user], 200);
    }


    public function forgotPasswordRequest(Request $request)
    {
        $details = ['phone_number' => $request->phone_number];

        $validator = Validator::make($details, [
            'phone_number' => 'required|exists:users,phone_number',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        try {
            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'user_type' => 'customer'
            ])->first();

            if (is_null($user)) {
                $message = 'User not found';
                $data = [];
                return response()->json(['success' => false, 'message' => $message, 'data' => $data], 200);
            }

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 6);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);

            $phone_number = $request->phone_number;
            $message = 'Your OTP Code is ' . $otp_code . ' Please enter this code to verify your mobile number.';
            $smsResponse = $this->sendSMS($request->phone_number, $message);
            $data = [
                'phone_number' => $user->phone_number,
                'code' => $otp_code,
            ];

            return response()->json(['success' => true, 'message' => $message, 'data' => $data, 'is_otp_send' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function resetPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|exists:users,phone_number',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        try {

            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'user_type' => 'customer'
            ])->first();

            if (is_null($user) || is_null($user->phone_number) || empty($user->phone_number)) {
                return response()->json(["success" => false, "message" => 'Your account is not associated with this mobile number.', 'data' => []], 200);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(["success" => true, "message" => 'Your password has been changes successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function sentOtpCode($phone_number)
    {
        $user = User::where([
            'phone_number' => $phone_number,
            'user_type' => 'customer'
        ])->first();

        if (is_null($user->otp_code)) {
            $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 6);
        } else {
            $otp_code = $user->otp_code;
        }

        $user->update([
            'otp_code' => $otp_code,
        ]);
        $message = 'Your OTP Code is ' . $otp_code . ' Please enter this code to verify your mobile number. This message is from Saver Buddy';
//        event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
        return $user;
    }


    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        try {

            $user = Auth::user();
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $name_with_time = time() . '_' . $name;
            $image->move($destinationPath, $name_with_time);
            $profile_image = $name_with_time;


            $user->update([
                'profile_pic' => $profile_image,
            ]);
            $user = new UserResource($user);
            return response()->json(['success' => true, 'message' => 'Profile image updated successfully.', 'data' => $user], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'date_of_birth' => 'required',
            'nationality' => 'required',
            'occupation' => 'required',
            'institution_name' => 'required',
            'address' => 'required',
            'address_two' => 'required',
            'region_capital' => 'required',
            'country' => 'required',
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        try {

            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'date_of_birth' => $request->get('date_of_birth'),
                'nationality' => $request->get('nationality'),
                'occupation' => $request->get('occupation'),
                'institution_name' => $request->get('institution_name'),
                'address' => $request->get('address'),
                'address_two' => $request->get('address_two'),
                'region_capital' => $request->get('region_capital'),
                'country' => $request->get('country'),
                'phone_number' => $request->get('phone_number'),
                'email' => $request->get('email'),
                'fcm_token' => $request->get('fcm_token'),
            ]);


            return response()->json(['success' => true, 'message' => 'Profile updated successfully.', 'data' => new UserResource($user)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
            'new-password-confirm' => 'required|string|min:6',

        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(['success' => false, 'message' => 'Your current password does not matches with the password you provided. Please try again.', 'data' => []], 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(['success' => false, 'message' => 'New Password cannot be same as your current password. Please choose a different password.', 'data' => []], 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(['success' => true, 'message' => 'Password Changed successfully.', 'data' => []], 200);

            } else {
                return response()->json(['success' => false, 'message' => 'New Password must be same as your confirm password.', 'data' => []], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }

    public function updateProfileNotificationSetting(Request $request)
    {

        $user = Auth::user();

        try {

            $user->update([
                'new_promotions' => $request->get('new_promotions') === true ? 1 : 0,
                'update_promotions' => $request->get('update_promotions') === true ? 1 : 0,
                'new_orders' => $request->get('new_orders') === true ? 1 : 0,
            ]);


            return response()->json(['success' => true, 'message' => 'Profile notification settings updated successfully.', 'data' => new UserResource($user)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function getNotifications()
    {
        $notifications = Notification::where('customer_id', Auth::id())->get();
        return response()->json(['success' => true, 'message' => 'Notifications fetched successfully', 'data' => $notifications], 200);
    }
}
