<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Vendor Routes
Route::post('vendor/verify-vendor', 'Api\Vendor\AuthController@verifyVendor');
Route::post('vendor/login', 'Api\Vendor\AuthController@login');
Route::post('vendor/verify/otp', 'Api\Vendor\AuthController@verifyOTP');
Route::post('vendor/forgot-password-request', 'Api\Vendor\AuthController@forgotPasswordRequest');
Route::post('vendor/reset-password', 'Api\Vendor\AuthController@resetPassword');
Route::get('settings', 'Api\GeneralController@settings');
Route::get('sliders', 'Api\Vendor\GeneralController@sliders');

//Customer Apis
Route::post('customer/login', 'Api\Customer\AuthController@login');
Route::post('customer/register', 'Api\Customer\AuthController@register');
Route::post('customer/verify/otp', 'Api\Customer\AuthController@verifyOTP');
Route::post('customer/forgot-password-request', 'Api\Customer\AuthController@forgotPasswordRequest');
Route::post('customer/reset-password', 'Api\Customer\AuthController@resetPassword');


Route::get('cities','Api\Customer\GeneralController@cities');


Route::group(['middleware' => 'auth:api'], function () {
    //Vendor Routes
    Route::post('vendor/logout', 'Api\Vendor\AuthController@logout');
    Route::get('vendor/user', 'Api\Vendor\AuthController@getUser');
    Route::post('vendor/update-profile', 'Api\Vendor\AuthController@updateProfile');
    Route::post('vendor/update-password', 'Api\Vendor\AuthController@updatePassword');
    Route::post('vendor/update-profile-image', 'Api\Vendor\AuthController@updateProfileImage');

    // Vendor Ads
    Route::post('vendor/promotion/create', 'Api\Vendor\PromotionController@create');
    Route::get('vendor/promotion/listing', 'Api\Vendor\PromotionController@listing');
    Route::get('vendor/promotion/findById/{id}', 'Api\Vendor\PromotionController@findById');
    Route::post('vendor/promotion/update/{id}', 'Api\Vendor\PromotionController@update');
    Route::post('vendor/promotion/delete/{id}', 'Api\Vendor\PromotionController@delete');


    //User Verification Controller
    Route::post('vendor/verify-user', 'Api\Vendor\UserVerificationController@verifyUser');
    Route::post('vendor/send-otpcode', 'Api\Vendor\UserVerificationController@sendOtpCode');
    Route::post('vendor/verify-otpcode', 'Api\Vendor\UserVerificationController@verifyOtpCode');

    //  Create Order
    Route::post('vendor/create-order', 'Api\Vendor\GeneralController@createOrder');
    Route::get('vendor/order-history', 'Api\Vendor\GeneralController@orderHistory');

    //Customer Routes
    Route::post('customer/logout', 'Api\Customer\AuthController@logout');
    Route::get('customer/user', 'Api\Customer\AuthController@getUser');
    Route::post('customer/update-profile', 'Api\Customer\AuthController@updateProfile');
    Route::post('customer/update-password', 'Api\Customer\AuthController@updatePassword');
    Route::post('customer/update-profile-image', 'Api\Customer\AuthController@updateProfileImage');
    Route::post('customer/update-profile-notification-setting', 'Api\Customer\AuthController@updateProfileNotificationSetting');
    Route::get('customer/notifications', 'Api\Customer\AuthController@getNotifications');

    Route::post('customer/add-vendor-to-favourite', 'Api\Customer\GeneralController@addVendorToFavourite');
    Route::post('customer/remove-vendor-to-favourite', 'Api\Customer\GeneralController@removeVendorToFavourite');
    Route::post('customer/add-promotion-to-favourite', 'Api\Customer\GeneralController@addPromotionToFavourite');
    Route::post('customer/remove-promotion-to-favourite', 'Api\Customer\GeneralController@removePromotionToFavourite');


    Route::get('customer/favourites-list', 'Api\Customer\GeneralController@favouriteList');

});


Route::get('customer/featured-vendors', 'Api\Customer\GeneralController@featuredVendors');
Route::get('customer/featured-promotions', 'Api\Customer\GeneralController@featuredPromotions');
Route::get('customer/promotions', 'Api\Customer\GeneralController@allPromotions');
Route::get('customer/vendors', 'Api\Customer\GeneralController@allVendors');
Route::get('customer/filter-vendors-and-promotions', 'Api\Customer\GeneralController@filterVendorsAndPromotions');
Route::get('customer/promotion-by-id/{id}', 'Api\Customer\GeneralController@promotionById');
Route::get('customer/categories', 'Api\Customer\GeneralController@categories');
Route::get('customer/filter-vendors-and-promotions-by-category/{id}', 'Api\Customer\GeneralController@filterVendorsAndPromotionsByCategory');
Route::get('customer/all-promotions-by-vendor-id/{id}', 'Api\Customer\GeneralController@allPromotionsByVendorId');
Route::get('customer/filter-vendors-and-promotions-by-city', 'Api\Customer\GeneralController@filterVendorsAndPromotionsByCity');



Route::fallback(function () {
    return response()->json(['message' => 'URL Not Found'], 404);
});
