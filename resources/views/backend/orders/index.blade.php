@extends('backend.layouts.master')
@section('title','Orders')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
    <style>
        .responsiveTable {
            overflow-x: scroll;
        }

        @media screen and (max-width: 768px) {
            .responsiveTable {
                overflow-x: scroll;
            }
        }

        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

        }

        /*    loading css*/

        .overlay {
            left: 0 !important;
            top: 0 !important;
            width: 100% !important;
            height: 100% !important;
            position: fixed !important;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 99999;
        }

        .overlay__inner {
            left: 0 !important;
            top: 0 !important;
            width: 100% !important;
            height: 100% !important;
            position: absolute !important;
        }

        .overlay__content {
            left: 50% !important;
            position: absolute !important;
            top: 50% !important;
            transform: translate(-50%, -50%) !important;
        }

        .spinner {
            width: 75px !important;
            height: 75px !important;
            display: inline-block !important;
            border-width: 2px !important;
            border-color: rgba(255, 255, 255, 0.05) !important;
            border-top-color: #fff !important;
            animation: spin 1s infinite linear !important;
            border-radius: 100% !important;
            border-style: solid !important;
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
@endpush
@section('content')
    <div class="overlay" style="display: none">
        <div class="overlay__inner">
            <div class="overlay__content"><span class="spinner"></span></div>
        </div>
    </div>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"><span>   Order Histories</span></h5>
            <div class="table-responsive text-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="text-nowrap">

                        <th> Customer name</th>
                        <th> Discount percentage</th>
                        <th> Discount price</th>
                        <th> VAT percentage</th>
                        <th> VAT price</th>
                        <th> Grand total</th>
                        <th> Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($orders))
                        @foreach($orders as $order)
                            <tr>
                                <td> {{ $order->customer ? $order->customer->fullName() : '-' }}</td>
                                <td> {{ $order->discount_percentage }}</td>
                                <td> {{ $order->discount_price }}</td>
                                <td> {{ $order->vat_percentage }}</td>
                                <td> {{ $order->vat_price }}</td>
                                <td> {{ $order->grand_total }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                data-bs-toggle="dropdown">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="javascript:void(0)"
                                               data-order="{{$order}}"
                                               data-order-date="{{date('d-m-Y h:i:s A', strtotime($order->created_at))}}"
                                               data-customer="{{ $order->customer ? $order->customer->fullName() : '--' }}"
                                               class="dropdown-item view-detail"><i class="bx bxs-eyedropper me-1"></i>
                                                View Detail</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')
    <div class="modal fade" id="backDropModal" data-bs-backdrop="static" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="backDropModalTitle">Order Detail</h5>
                    <button
                        type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div class="modal-body">

                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="customer" class="form-label">Customer Name</label>
                            <input
                                readonly
                                type="text"
                                id="customer"
                                class="form-control"
                                placeholder="Enter Name"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="customer_phone_number" class="form-label">Customer Phone number</label>
                            <input
                                readonly
                                type="text"
                                id="customer_phone_number"
                                class="form-control"
                                placeholder="xxxx@xxx.xx"
                            />
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="discount_percentage" class="form-label">Discounted Percentage</label>
                            <input
                                readonly
                                type="text"
                                id="discount_percentage"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="discount_price" class="form-label">Discounted Price</label>
                            <input
                                readonly
                                type="text"
                                id="discount_price"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>

                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="vat_percentage" class="form-label">Vat Percentage</label>
                            <input
                                readonly
                                type="text"
                                id="vat_percentage"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="vat_price" class="form-label">VAT Price</label>
                            <input
                                readonly
                                type="text"
                                id="vat_price"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="grand_total" class="form-label">Grand Total</label>
                            <input
                                readonly
                                type="text"
                                id="grand_total"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="order_date" class="form-label">Order Date</label>
                            <input
                                readonly
                                type="text"
                                id="order_date"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea
                                readonly
                                type="text"
                                id="description"
                                class="form-control"
                                placeholder="Enter Name"
                            ></textarea>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                </div>
            </form>
        </div>
    </div>
@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: []}
            ],
        });
        $(document).on('click', '.view-detail', function () {
            var order = $(this).data('order');
            $('#customer').val($(this).data('customer'));
            $('#customer_phone_number').val(order.customer_phone_number);
            $('#description').val(order.description);
            $('#discount_percentage').val(order.discount_percentage);
            $('#discount_price').val(order.discount_price);
            $('#vat_percentage').val(order.vat_percentage);
            $('#vat_price').val(order.vat_price);
            $('#grand_total').val(order.grand_total);
            $('#order_date').val($(this).data('order-date'));
            $('#backDropModal').modal('show');
        });
    </script>
@endpush
