<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Api\QueryException;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\PaginatedPromotionResource;
use App\Http\Resources\PromotionResource;
use App\Http\Resources\UserResource;
use App\Models\Category;
use App\Models\FavouritePromotions;
use App\Models\FavouriteVendor;
use App\Models\Promotion;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{

    public function featuredVendors()
    {

        try {

            $featuredVendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
                'is_featured' => 1
            ])->get();
            $data['featured_vendors'] = UserResource::collection($featuredVendors);
            return response()->json(['success' => true, 'message' => 'Featured Vendors fetched successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function featuredPromotions()
    {
        try {

            $featuredPromotions = Promotion::where([
                'status' => 'active',
                'is_featured' => 1
            ])->get();
            $data['featured_promotions'] = PromotionResource::collection($featuredPromotions);
            return response()->json(['success' => true, 'message' => 'Featured Promotions fetched successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function allPromotions(Request $request)
    {
        try {

            $promotions = Promotion::where([
                'status' => 'active',
            ])->orderBy('created_at', 'desc');

            $data['promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            return response()->json(['success' => true, 'message' => 'All promotions fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function allVendors(Request $request)
    {
        try {

            $vendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
            ])->orderBy('created_at', 'desc')->get();

            $data['vendors'] = UserResource::collection($vendors);
            return response()->json(['success' => true, 'message' => 'All vendors fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function filterVendorsAndPromotions(Request $request)
    {
        try {

            $featuredPromotions = Promotion::where([
                'status' => 'active',
                'is_featured' => 1
            ])->when(request('name'), function ($q) {
                return $q->where('company_name', 'LIKE', '%' . request('name') . '%');
            })->get();

            $featuredVendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
                'is_featured' => 1
            ])->when(request('name'), function ($q) {
                return $q->where('first_name', 'LIKE', '%' . request('name') . '%')->orWhere('last_name', 'LIKE', '%' . request('name') . '%');
            })->get();


            $promotions = Promotion::where([
                'status' => 'active',
            ])->when(request('name'), function ($q) {
                return $q->where('company_name', 'LIKE', '%' . request('name') . '%');
            })->orderBy('created_at', 'desc');


            $vendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
            ])->when(request('name'), function ($q) {
                return $q->where('first_name', 'LIKE', '%' . request('name') . '%')->orWhere('last_name', 'LIKE', '%' . request('name') . '%');
            })->orderBy('created_at', 'desc')->get();


            $data['featured_vendors'] = UserResource::collection($featuredVendors);
            $data['featured_promotions'] = PromotionResource::collection($featuredPromotions);
            $data['promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            $data['vendors'] = UserResource::collection($vendors);
            return response()->json(['success' => true, 'message' => 'Filters vendors and promotions fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function promotionById($id)
    {
        try {

            $promotion = Promotion::where([
                'status' => 'active',
                'id' => $id
            ])->first();

            $data['promotion'] = new PromotionResource($promotion);
            return response()->json(['success' => true, 'message' => 'Promotion by ID fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function categories()
    {
        try {

            $categories = Category::where([
                'is_available' => 1,
            ])->get();

            $data['categories'] = CategoryResource::collection($categories);
            return response()->json(['success' => true, 'message' => 'Categories fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function filterVendorsAndPromotionsByCategory($id)
    {
        try {

            $featuredPromotions = Promotion::where([
                'status' => 'active',
                'is_featured' => 1,
                'category_id' => $id
            ])->get();

            $featuredVendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
                'is_featured' => 1,
                'category_id' => $id
            ])->get();


            $promotions = Promotion::where([
                'status' => 'active',
                'category_id' => $id
            ])->orderBy('created_at', 'desc');

            $data['featured_vendors'] = UserResource::collection($featuredVendors);
            $data['featured_promotions'] = PromotionResource::collection($featuredPromotions);
            $data['promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            return response()->json(['success' => true, 'message' => 'Filters vendors and promotions by category fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function addVendorToFavourite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            FavouriteVendor::create([
                'vendor_id' => $request->vendor_id,
                'customer_id' => Auth::id()
            ]);
            return response()->json(['success' => true, 'message' => 'Vendor added to favourite successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function removeVendorToFavourite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vendor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            FavouriteVendor::where([
                'vendor_id' => $request->vendor_id,
                'customer_id' => Auth::id()
            ])->delete();
            return response()->json(['success' => true, 'message' => 'Vendor removed from favourite successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function addPromotionToFavourite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promotion_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            FavouritePromotions::create([
                'promotion_id' => $request->promotion_id,
                'customer_id' => Auth::id()
            ]);
            return response()->json(['success' => true, 'message' => 'Promotion added to favourite successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function removePromotionToFavourite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promotion_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            FavouritePromotions::where([
                'promotion_id' => $request->promotion_id,
                'customer_id' => Auth::id()
            ])->delete();
            return response()->json(['success' => true, 'message' => 'Promotion removed from favourite successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function favouriteList(Request $request)
    {
        try {

            $favouriteVendors = FavouriteVendor::where('customer_id', Auth::id())->pluck('vendor_id')->toArray();
            $favouritePromotions = FavouritePromotions::where('customer_id', Auth::id())->pluck('promotion_id')->toArray();
            $featuredVendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
            ])->whereIn('id', $favouriteVendors)->get();

            $promotions = Promotion::where([
                'status' => 'active',
            ])->whereIn('id', $favouritePromotions)->orderBy('created_at', 'desc');

            $data['favourite_vendors'] = UserResource::collection($featuredVendors);
            $data['favourite_promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            return response()->json(['success' => true, 'message' => 'Favourite vendors and promotions fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    public function settings()
    {
        try {

            return response()->json(['data' => Setting::get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function filterVendorsAndPromotionsByCity(Request $request)
    {
        try {

            $featuredPromotions = Promotion::where([
                'status' => 'active',
                'is_featured' => 1
            ])->when(request('city'), function ($q) {
                return $q->where('city', 'LIKE', '%' . request('city') . '%');
            })->get();

            $featuredVendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
                'is_featured' => 1
            ])->when(request('city'), function ($q) {
                return $q->where('city', 'LIKE', '%' . request('city') . '%');
            })->get();


            $promotions = Promotion::where([
                'status' => 'active',
            ])->when(request('city'), function ($q) {
                return $q->where('city', 'LIKE', '%' . request('city') . '%');
            })->orderBy('created_at', 'desc');


            $vendors = User::where([
                'user_type' => 'vendor',
                'status' => 'verified',
            ])->when(request('city'), function ($q) {
                return $q->where('city', 'LIKE', '%' . request('city') . '%');
            })->orderBy('created_at', 'desc')->get();


            $data['featured_vendors'] = UserResource::collection($featuredVendors);
            $data['featured_promotions'] = PromotionResource::collection($featuredPromotions);
            $data['promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            $data['vendors'] = UserResource::collection($vendors);
            return response()->json(['success' => true, 'message' => 'Filters vendors and promotions fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function cities(){
        try {

            $cities = [
                'Accra', 'Kumasi', 'Tamale', 'Sekondi', 'Obuase', 'Tema', 'Cape Coast', 'Koforidua', 'Ho', 'Wa', 'Bawku', 'Sunyani', 'Bolgatanga', 'Aflao', 'Nkawkaw', 'Hohoe', 'Winneba', 'Berekum', 'Techiman', 'Sefwi Wiawso', 'Goaso', 'Dambai', 'Nalerigu', 'Damongo'
            ];

            return response()->json(['success' => true, 'message' => 'Cities fetched successfully.', 'data' => $cities], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }


    public function allPromotionsByVendorId($id)
    {
        try {

            $promotions = Promotion::where([
                'status' => 'active',
                'vendor_id' => $id,
            ])->orderBy('created_at', 'desc');

            $data['promotions'] = new PaginatedPromotionResource($promotions->paginate(20));
            return response()->json(['success' => true, 'message' => 'All Promotions by vendor id fetch successfully.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }



}
