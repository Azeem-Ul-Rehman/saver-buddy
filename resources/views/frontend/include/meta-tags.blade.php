@if(Route::currentRouteName() == 'service.items' || Route::currentRouteName() == 'category.blogs' ||  Route::currentRouteName() == 'package.show' || Route::currentRouteName() == 'blog.show')
    <title>Saver Buddy | @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
@elseif(is_null($meta_information))
    <title>Saver Buddy | @yield('title')</title>
    <meta name="description"
          content="Find The Best Discount Deals In Ghana Daily!">
    <meta name="keywords" content="Saver Buddy">
@else
    <title>Saver Buddy | {{$meta_information->title}}</title>
    <meta name="description" content="{{ $meta_information->description }}">
    <meta name="keywords" content="{{ $meta_information->keywords }}">
@endif

<meta name="msapplication-TileImage" content="https://saverbuddy.app/wp-content/uploads/2023/03/cropped-SAVERBUDDY-LOGO_221208_140636-270x270.png">


