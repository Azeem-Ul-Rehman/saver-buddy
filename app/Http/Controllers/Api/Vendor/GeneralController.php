<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Http\Controllers\Controller;

use App\Http\Resources\OrderHistoryPaginatedResource;
use App\Http\Resources\OrderHistoryResource;
use App\Http\Resources\SliderResource;
use App\Jobs\SendQueueNotification;
use App\Models\Notification;
use App\Models\OrderHistory;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\User;
use App\Traits\GeneralHelperTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GeneralController extends Controller
{

    use GeneralHelperTrait;

    public function settings()
    {
        try {

            return response()->json(['data' => Setting::get()], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function createOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'customer_phone_number' => 'required',
            'description' => 'required',
            'order_total' => 'required',
            'discount_percentage' => 'required',
            'discount_price' => 'required',
            'total' => 'required',
            'vat_percentage' => 'required',
            'vat_price' => 'required',
            'grand_total' => 'required'
        ], [
            'customer_id.required' => 'Customer ID is required',
            'customer_phone_number.required' => 'Customer Phone number is required',
            'description.required' => 'Description is required',
            'order_total.required' => 'Order total is required',
            'discount_percentage.required' => 'Discount percentage is required',
            'discount_price.required' => 'Discount price is required',
            'total.required' => 'Total is required',
            'vat_percentage.required' => 'Vat percentage is required',
            'vat_price.required' => 'Vat price is required',
            'grand_total.required' => 'Grand total is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()->first()], 200);
        }
        $order = OrderHistory::create([
            'customer_id' => $request->get('customer_id'),
            'customer_phone_number' => $request->get('customer_phone_number'),
            'vendor_id' => Auth::id(),
            'description' => $request->get('description'),
            'order_total' => $request->get('order_total'),
            'discount_percentage' => $request->get('discount_percentage'),
            'discount_price' => $request->get('discount_price'),
            'total' => $request->get('total'),
            'vat_percentage' => $request->get('vat_percentage'),
            'vat_price' => $request->get('vat_price'),
            'grand_total' => $request->get('grand_total'),
        ]);
        $this->sendFCMNotification($request->get('customer_id'), Auth::user()->fullName(), $request->get('description'));


        return response()->json(['success' => true, 'message' => 'Record has been submitted successfully.', 'data' => new OrderHistoryResource($order)], 200);


    }

    public function orderHistory()
    {
        $orderHistories = OrderHistory::where('vendor_id', Auth::id())->orderBy('created_at', 'desc');
        $data['order_histories'] = new OrderHistoryPaginatedResource($orderHistories->paginate(20));
        return response()->json(['success' => true, 'message' => 'Orders histories fetch successfully.', 'data' => $data], 200);
    }

    public function sliders(Request $request)
    {
        $sliders = SliderResource::collection(Slider::whereStatus('active')->orderBy('id', 'ASC')->get());
        return response()->json(['status' => true, 'data' => $sliders, 'message' => 'Slider images get successfully.'], 200);

    }
    public function sendFCMNotification($customer_id, $vendor_name, $description)
    {
        $details = [
            'title' => 'New Order',
            'message' => "A new order has been created by vendor " . $vendor_name . ' ' . $description,
            'new_orders' => 'new_promotions',
            'is_order' => true,
            'customer_id' => $customer_id
        ];
        $job = (new SendQueueNotification($details))->delay(now()->addSeconds(2));
        dispatch($job);


    }


}
