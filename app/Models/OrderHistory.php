<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = "order_histories";
    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }
    public function vendor(){
        return $this->belongsTo(User::class, 'vendor_id', 'id');
    }
}
