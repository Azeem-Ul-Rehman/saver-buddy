<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CategoryResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "name" => $this->name,
            "slug" => $this->slug,
            "is_available" => $this->is_available === 1 ? 'Active' : 'In-active',
            "image" => asset('/uploads/service_category/thumbnails/' . $this->thumbnail_image),
            "description" => $this->description,

        ];
    }
}
