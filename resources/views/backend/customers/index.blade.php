@extends('backend.layouts.master')
@section('title','Customers')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"><span>Customers</span></h5>
            <div class="table-responsive text-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="text-nowrap">

                        <th> Full Name</th>
                        <th> Email</th>
                        <th style="width: 13% !important; "> Date of birth</th>
                        <th> Occupation</th>
                        <th> Mobile Number</th>
                        <th> Status</th>
                        <th style="width: 11% !important; "> Subscription Status</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($users))
                        @foreach($users as $user)
                            @if($user->user_type != 'admin')
                                <tr>
                                    <td>{{ucfirst($user->fullname())}}</td>
                                    <td>{{$user->email}}</td>
                                    <td style="width: 13% !important;">{{$user->date_of_birth}}</td>
                                    <td>{{$user->occupation}}</td>
                                    <td>{{$user->phone_number}}</td>
                                    <td style="width: 10%;">{{ucfirst($user->status)}}</td>
                                    <td style="width: 13% !important;">
                                        <select
                                            id="inputGroupSelect02"
                                            class="form-select update-subscription-status @error('subscription_status') is-invalid @enderror"
                                            name="subscription_status" autocomplete="subscription_status"
                                            data-customer-id="{{$user->id}}">
                                            <option
                                                value="active" {{ $user->subscription_status === 'active' ? 'selected' : '' }}>
                                                Active
                                            </option>
                                            <option
                                                value="inactive" {{ $user->subscription_status === 'inactive' ? 'selected' : '' }}>
                                                Inactive
                                            </option>
                                        </select>
                                    </td>
                                    <td nowrap style="width: 28%;">
                                        <div class="dropdown">
                                            <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                    data-bs-toggle="dropdown">
                                                <i class="bx bx-dots-vertical-rounded"></i>
                                            </button>
                                            <div class="dropdown-menu">

                                                <form method="post"
                                                      action="{{ route('admin.customers.destroy', $user->id) }}"
                                                      id="delete_{{ $user->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="dropdown-item"
                                                       href="javascript:void(0)"
                                                       onclick="if(confirmDelete()){ document.getElementById('delete_<?=$user->id?>').submit(); }">
                                                        <i class="bx bx-trash me-1"></i> Delete
                                                    </a>
                                                </form>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });
        $(document).on('change', '.update-subscription-status', function () {

            form = $(this).closest('form');
            node = $(this);
            var subscription_status = $(this).val();
            var customer_id = $(this).data('customer-id');
            var request = {"id": customer_id, "subscription_status": subscription_status};
            if (customer_id !== '') {
                $('.overlay').show();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.update.customer-subscription-status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        $('.overlay').hide();
                        if (response.status == "success") {
                            toastr['success']("Subscription Status Updated Successfully");

                        }
                    },
                    error: function () {
                        $('.overlay').hide();
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Order status");
            }

        });

    </script>
@endpush
