@extends('backend.layouts.master')
@section('title','Vendors')
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"> Edit {{ __('Vendor') }}</h5>
            <div class="card-body demo-vertical-spacing demo-only-element">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('admin.users.update',$user->id) }}"
                              enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body mb-3">
                                <div class="m-form__section m-form__section--first  mb-3">

                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="first_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="first_name" type="text"
                                                   class="form-control @error('first_name') is-invalid @enderror"
                                                   name="first_name" value="{{ $user->first_name }}"
                                                   autocomplete="first_name" autofocus>

                                            @error('first_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="last_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="last_name" type="text"
                                                   class="form-control @error('last_name') is-invalid @enderror"
                                                   name="last_name" value="{{ $user->last_name }}"
                                                   autocomplete="last_name" autofocus>

                                            @error('last_name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">

                                            <label for="role_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Role') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <select id="role_id"
                                                    class="form-control roles @error('role_id') is-invalid @enderror"
                                                    name="role_id" autocomplete="role_id">
                                                @if(!empty($user->roles))
                                                    @foreach($user->roles as $role)
                                                        <option
                                                            value="{{$role->id}}" {{ ($role->id == $user->role_id) ? 'selected' : ''}}
                                                        >{{ucfirst($role->name)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('role_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Category') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control roles @error('category_id') is-invalid @enderror"
                                                    name="category_id" autocomplete="category_id">
                                                <option value="">Select an category</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{$category->id}}" {{ (old('category_id',$user->category_id) == $category->id) ? 'selected' : '' }}>{{ucfirst($category->name)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="city"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Cities') }}
                                                <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="city"
                                                    class="form-control roles @error('city') is-invalid @enderror"
                                                    name="city" autocomplete="city">
                                                @if(!empty($cities))
                                                    @foreach($cities as $city)
                                                        <option
                                                            value="{{$city}}" {{ (old('city',$user->city) == $city) ? 'selected' : '' }}>{{ucfirst($city)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-4">
                                            <label for="email"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="email" type="email"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ $user->email }}" autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="phone_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="phone_number" type="text"
                                                   class="form-control @error('phone_number') is-invalid @enderror"
                                                   name="phone_number" value="{{ $user->phone_number }}"
                                                   autocomplete="phone_number" autofocus placeholder="03001234567"
                                                   pattern="[03]{2}[0-9]{9}"
                                                   maxlength="11"
                                                   onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                                   title="Phone number with 03 and remaing 9 digit with 0-9">

                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="password"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Password') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password"
                                                   autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="short_description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Short Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="short_description" type="text"
                                                   class="form-control @error('short_description') is-invalid @enderror"
                                                   name="short_description"
                                                   value="{{ old('short_description',$user->short_description ) }}"
                                                   autocomplete="short_description">

                                            @error('short_description')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>

                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ $user->address }}" autocomplete="address">

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="status"
                                                    class="form-control cities @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status">
                                                <option value="">Select an option</option>
                                                <option
                                                    value="pending" {{ ($user->status == 'pending') ? 'selected' : '' }}>
                                                    Pending
                                                </option>
                                                <option
                                                    value="verified" {{ ($user->status == 'verified') ? 'selected' : '' }}>
                                                    Verified
                                                </option>
                                                <option
                                                    value="suspended" {{ ($user->status == 'suspended') ? 'selected' : '' }}>
                                                    Suspended
                                                </option>
                                            </select>

                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="is_featured"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Is featured') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="status"
                                                    class="form-control cities @error('is_featured') is-invalid @enderror"
                                                    name="is_featured" autocomplete="is_featured">
                                                <option value="0" {{ $user->is_featured === 0 ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ $user->is_featured === 1 ? 'selected' : '' }}>Yes</option>
                                            </select>

                                            @error('is_featured')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="subscription_status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Subscription status') }} <span
                                                    class="mandatorySign">*</span></label>

                                            <select id="subscription_status"
                                                    class="form-control cities @error('subscription_status') is-invalid @enderror"
                                                    name="subscription_status" autocomplete="subscription_status">
                                                <option
                                                    value="active" {{ ($user->subscription_status == 'active') ? 'selected' : '' }}>
                                                    Active
                                                </option>
                                                <option
                                                    value="inactive" {{ ($user->subscription_status == 'inactive') ? 'selected' : '' }}>
                                                    Inactive
                                                </option>
                                            </select>

                                            @error('subscription_status')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}</label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($user->profile_pic) ? 'block' : 'none'}};"
                                                 id="img"
                                                 src="{{ asset('/uploads/user_profiles/'.$user->profile_pic) }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image_one"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Image 1') }}</label>
                                            <input value="{{old('image_one')}}" type="file"
                                                   class="form-control @error('image_one') is-invalid @enderror"
                                                   onchange="readURLImageOne(this)" id="image_one"
                                                   name="image_one" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($user->image_one) ? 'block' : 'none'}};"
                                                 id="img_one"
                                                 src="{{ asset('/uploads/user_profiles/'.$user->image_one) }}"
                                                 alt="your image"/>

                                            @error('image_one')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="image_two"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Image 2') }}</label>
                                            <input value="{{old('image_two')}}" type="file"
                                                   class="form-control @error('image_two') is-invalid @enderror"
                                                   onchange="readURLImageTwo(this)" id="image_two"
                                                   name="image_two" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($user->image_two) ? 'block' : 'none'}};"
                                                 id="img_two"
                                                 src="{{ asset('/uploads/user_profiles/'.$user->image_two) }}"
                                                 alt="your image"/>

                                            @error('image_two')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="image_three"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Image 3') }}</label>
                                            <input value="{{old('image_three')}}" type="file"
                                                   class="form-control @error('image_three') is-invalid @enderror"
                                                   onchange="readURLImageThree(this)" id="image_three"
                                                   name="image_three" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($user->image_three) ? 'block' : 'none'}};"
                                                 id="img_three"
                                                 src="{{ asset('/uploads/user_profiles/'.$user->image_three) }}"
                                                 alt="your image"/>

                                            @error('image_three')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="image_four"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Image 4') }}</label>
                                            <input value="{{old('image_four')}}" type="file"
                                                   class="form-control @error('image_four') is-invalid @enderror"
                                                   onchange="readURLImageFour(this)" id="image_four"
                                                   name="image_four" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($user->image_four) ? 'block' : 'none'}};"
                                                 id="img_four"
                                                 src="{{ asset('/uploads/user_profiles/'.$user->image_four) }}"
                                                 alt="your image"/>

                                            @error('image_four')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right float-end">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.users.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>

        </div>

    </div>
@endsection


@push('js')
    <script>
        $('#name').focusout(function () {

            var name = $(this).val();
            name = name.replace(/\s+/g, '-').toLowerCase();

            $('#slug').val(name);
        })


    </script>
    <script>
        function readURL(input) {
            console.log(input);
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);

                var reader = new FileReader();
                reader.onload = function (e) {


                    if (size > 2000000) {
                        $('#img').attr('src', '');
                        $('#image').val('');
                        toastr.warning(" Image Size is exceeding 2 Mb");


                    } else {
                        $('#img').attr('src', e.target.result);
                        $('#img').css("display", "block");
                        $('#hidden-field').val('');
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLImageOne(input) {
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);



                var reader = new FileReader();
                reader.onload = function (e) {

                    if (size > 2000000) {
                        $('#img_one').attr('src', '');
                        $('#image_one').val('');
                        toastr.warning("Thumbnail Image Size is exceeding 2 Mb");
                    } else {
                        $('#img_one').attr('src', e.target.result).css("display", "block");
                        $('#hidden-field').val('');
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLImageTwo(input) {
            console.log(input);
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);

                var reader = new FileReader();
                reader.onload = function (e) {


                    if (size > 2000000) {
                        $('#img_two').attr('src', '');
                        $('#image_two').val('');
                        toastr.warning(" Image Size is exceeding 2 Mb");


                    } else {
                        $('#img_two').attr('src', e.target.result).css("display", "block");
                        $('#hidden-field').val('');
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLImageThree(input) {
            console.log(input);
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);

                var reader = new FileReader();
                reader.onload = function (e) {


                    if (size > 2000000) {
                        $('#img_three').attr('src', '');
                        $('#image_three').val('');
                        toastr.warning(" Image Size is exceeding 2 Mb");


                    } else {
                        $('#img_three').attr('src', e.target.result).css("display", "block");
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURLImageFour(input) {
            console.log(input);
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);

                var reader = new FileReader();
                reader.onload = function (e) {


                    if (size > 2000000) {
                        $('#img_four').attr('src', '');
                        $('#image_four').val('');
                        toastr.warning(" Image Size is exceeding 2 Mb");


                    } else {
                        $('#img_four').attr('src', e.target.result).css("display", "block");
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endpush


