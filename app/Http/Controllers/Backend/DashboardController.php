<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\OrderHistory;
use App\Models\Promotion;
use App\Models\User;
use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $total_orders = OrderHistory::count();
        $vendor_counts = User::where('user_type', 'vendor')->count();
        $customer_counts = User::where('user_type', 'customer')->count();
        $promotions = Promotion::count();
        $currentYearSale = OrderHistory::whereDate('created_at','>=', date('Y'))->sum('grand_total');
        $previousYearSale = OrderHistory::whereDate('created_at','>=', date('Y',strtotime("-1 year")))->whereDate('created_at','<=', date('Y'))->sum('grand_total');
        return view('backend.dashboard.index', compact('vendor_counts', 'total_orders', 'customer_counts', 'promotions', 'currentYearSale', 'previousYearSale'));
    }
}
