<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Http\Resources\UserResource;
use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

use DB;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    use GeneralHelperTrait;

    public $successStatus = 200;


    public function verifyVendor(Request $request)
    {
        $details = ['phone_number' => $request->phone_number];

        $validator = Validator::make($details, [
            'phone_number' => 'required|exists:users,phone_number',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => 'User not found', 'data' => []], 200);
        }

        try {
            $user = User::where('phone_number', $request->phone_number)->first();

            if (!is_null($user)) {
                $message = 'User found';
            } else {
                $message = 'User not found';
            }
            return response()->json(['success' => true, 'message' => $message, 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 200);
        }
    }

    public function login(Request $request)
    {
        $user = User::where([
            'phone_number' => $request->get('phone_number'),
            'user_type' => 'vendor'
        ])->first();

        if (!is_null($user)) {
            $validator = Validator::make($request->all(), [
                'phone_number' => 'required|exists:users,phone_number',
                'password' => 'required'
            ], [
                'phone_number.required' => 'Phone Number is required',
                'password.required' => 'Password is required',

            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()->first()], 200);
            }

            if (Auth::attempt(['phone_number' => request('phone_number'), 'password' => request('password')])) {
                $user = Auth::user();

                if ($user->status == 'suspended') {
                    return response()->json(['success' => false, 'message' => 'Your account is suspended.', 'data' => []], 200);
                }

                $success['token'] = $user->createToken('AppName')->accessToken;
                $success['user'] = new UserResource($user);
                return response()->json(['success' => true, 'message' => 'User is found', 'data' => $success], 200);
            } else {
                return response()->json(['success' => true, 'message' => 'Invalid Password', 'data' => []], 200);
            }

        } else {
            $message = 'User not found';
            return response()->json(['success' => false, 'message' => $message, 'data' => []], 200);

        }

    }

    public function logout()
    {


        try {
            $user = Auth::user();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }
            return response()->json(['success' => true, 'message' => '', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $exception], 200);
        }
    }

    public function getUser()
    {
        $user = new UserResource(Auth::user());
        return response()->json(['success' => true, 'message' => '', 'data' => $user], 200);
    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|exists:users,phone_number',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {


            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->save();

                return response()->json(['status' => true, 'message' => 'OTP Code verified successfully.'], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'Provided OTP code is not correct.'], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }


    public function forgotPasswordRequest(Request $request)
    {
        $details = ['phone_number' => $request->phone_number];

        $validator = Validator::make($details, [
            'phone_number' => 'required|exists:users,phone_number',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        try {

            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'user_type' => 'vendor'
            ])->first();

            if (is_null($user)) {
                $message = 'User not found';
                $data = [];
                return response()->json(['success' => false, 'message' => $message, 'data' => $data], 200);
            }

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 6);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);

            $phone_number = $request->phone_number;
            $message = 'Your OTP Code is ' . $otp_code . ' Please enter this code to verify your mobile number.';

            $data = [
                'phone_number' => $user->phone_number,
                'code' => $otp_code,
            ];

            $smsResponse = $this->sendSMS($phone_number, $message);

            return response()->json(['success' => true, 'message' => $message, 'data' => $data, 'is_otp_send' => true, 'smsResponse' => $smsResponse], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function resetPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|exists:users,phone_number',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        try {

            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'user_type' => 'vendor'
            ])->first();

            if (is_null($user) || is_null($user->phone_number) || empty($user->phone_number)) {
                return response()->json(["success" => false, "message" => 'Your account is not associated with this mobile number.', 'data' => []], 200);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(["success" => true, "message" => 'Your password has been changes successfully.', 'data' => []], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }


    public function updateProfileImage(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
//        }

        try {

            $user = Auth::user();



            if (isset($request->image) && $request->has('image')) {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $name_with_time = time() . '_' . $name;
                $image->move($destinationPath, $name_with_time);
                $profile_image = $name_with_time;
            } else {
                $profile_image = $user->profile_pic;
            }

            if (isset($request->image_one) && $request->has('image_one')) {
                $image = $request->file('image_one');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $finalName = rand() . $name;
                $image->move($destinationPath, $finalName);
                $image_one = $finalName;
            } else {
                $image_one = $user->image_one;
            }
            if (isset($request->image_two) && $request->has('image_two')) {
                $image = $request->file('image_two');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $finalName = rand() . $name;
                $image->move($destinationPath, $finalName);
                $image_two = $finalName;
            } else {
                $image_two = $user->image_two;
            }
            if (isset($request->image_three) && $request->has('image_three')) {
                $image = $request->file('image_three');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $finalName = rand() . $name;
                $image->move($destinationPath, $finalName);
                $image_three = $finalName;
            } else {
                $image_three = $user->image_three;
            }
            if (isset($request->image_four) && $request->has('image_four')) {
                $image = $request->file('image_four');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $finalName = rand() . $name;
                $image->move($destinationPath, $finalName);
                $image_four = $finalName;
            } else {
                $image_four = $user->image_four;
            }


            $user->update([
                'profile_pic' => $profile_image,
                'image_one' => $image_one,
                'image_two' => $image_two,
                'image_three' => $image_three,
                'image_four' => $image_four,
            ]);
            $user = new UserResource($user);
            return response()->json(['success' => true, 'message' => 'Profile image updated successfully.', 'data' => $user], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateProfile(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'date_of_birth' => 'required',
            'nationality' => 'required',
            'occupation' => 'required',
            'institution_name' => 'required',
            'address' => 'required',
            'address_two' => 'required',
            'region_capital' => 'required',
            'country' => 'required',
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        try {

            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'date_of_birth' => $request->get('date_of_birth'),
                'nationality' => $request->get('nationality'),
                'occupation' => $request->get('occupation'),
                'institution_name' => $request->get('institution_name'),
                'address' => $request->get('address'),
                'address_two' => $request->get('address_two'),
                'region_capital' => $request->get('region_capital'),
                'country' => $request->get('country'),
                'phone_number' => $request->get('phone_number'),
                'email' => $request->get('email'),
            ]);


            return response()->json(['success' => true, 'message' => 'Profile updated successfully.', 'data' => new \App\Http\Resources\Customer\UserResource($user)], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
            'new-password-confirm' => 'required|string|min:6',

        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }
        try {

            if (!(Hash::check($request->get('current-password'), $user->password))) {
                // The passwords matches
                return response()->json(['success' => false, 'message' => 'Your current password does not matches with the password you provided. Please try again.', 'data' => []], 200);
            }

            if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
                //Current password and new password are same
                return response()->json(['success' => false, 'message' => 'New Password cannot be same as your current password. Please choose a different password.', 'data' => []], 200);
            }

            if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
                //Change Password
                $user->password = bcrypt($request->get('new-password'));
                $user->save();

                return response()->json(['success' => true, 'message' => 'Password Changed successfully.', 'data' => []], 200);

            } else {
                return response()->json(['success' => false, 'message' => 'New Password must be same as your confirm password.', 'data' => []], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }


}
