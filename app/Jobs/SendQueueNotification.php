<?php

namespace App\Jobs;

use App\Models\Notification;
use App\Models\User;
use App\Traits\GeneralHelperTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendQueueNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;
    public $timeout = 7200; // 2 hours
    use GeneralHelperTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $title = $this->details['title'];
        $message = $this->details['message'];
        $id = 1;
        $type = "basic";
        if ($this->details['is_order'] === false) {
            $data = User::where('user_type', 'customer')->whereNotNull('fcm_token')->where($this->details['variable_name'], true)->get(['id', 'fcm_token', 'user_type', $this->details['variable_name']]);
            if (!empty($data) && count($data) > 0) {
                foreach ($data as $key => $value) {
                    $res = $this->send_notification_FCM($value->fcm_token, $title, $message, $id, $type);
                    if ($res === 1) {
                        Notification::create([
                            'customer_id' => $value->id,
                            'title' => $title,
                            'message' => $message,
                            'type' => $this->details['variable_name'] === 'new_promotions' ? 'new_promotions' : 'update_promotions',
                        ]);
                    }
                }
            }
        } else {
            $user = User::where('user_type', 'customer')->where('id', (int)$this->details['customer_id'])->whereNotNull('fcm_token')->where('new_orders', 1)->first(['id', 'fcm_token', 'user_type', 'new_orders']);

            if (!is_null($user)) {
                $notification_id = $user->fcm_token;
                $res = $this->send_notification_FCM($notification_id, $title, $message, $id, $type);
                if ($res === 1) {
                    Notification::create([
                        'customer_id' => $user->id,
                        'title' => $title,
                        'message' => $message,
                        'type' => 'new_orders',
                    ]);
                }
            }
        }


    }
}
