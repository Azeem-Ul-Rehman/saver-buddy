@extends('backend.layouts.master')
@section('title','Categories')
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"> Edit {{ __('Category') }}</h5>
            <div class="card-body demo-vertical-spacing demo-only-element">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('admin.category.update', $service_category->id) }}"
                              enctype="multipart/form-data">
                            @method('patch')
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first  mb-3">


                                    <div class="form-group row">


                                        <div class="col-md-4">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Name') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   name="name" value="{!! $service_category->name !!}"
                                                   autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label for="slug"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Slug') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="slug" type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug" value="{!! $service_category->slug !!}"
                                                   autocomplete="slug" autofocus>

                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label for="is_available"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }}</label>

                                            <select id="inputGroupSelect02"
                                                    class="form-select @error('is_available') is-invalid @enderror"
                                                    name="is_available" autocomplete="is_available">
                                                <option
                                                    value="0" {{ ($service_category->is_available == 0) ? 'selected' : ''  }}>
                                                    In-active
                                                </option>
                                                <option
                                                    value="1" {{ ($service_category->is_available == 1) ? 'selected' : ''  }}>
                                                    Active
                                                </option>
                                            </select>

                                            @error('is_available')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description"
                                                      autocomplete="description">{{ $service_category->description }}</textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="thumbnail_image"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input type="file"
                                                   class="form-control @error('thumbnail_image') is-invalid @enderror"
                                                   onchange="readURLThumbnail(this)"
                                                   name="thumbnail_image" style="padding: 9px; cursor: pointer"
                                                   id="thumbnail_image"
                                                   value="{{$service_category->thumbnail_image}}">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:{{($service_category->thumbnail_image) ? 'block' : 'none'}};"
                                                 id="img_thumbnail"
                                                 src="{{ asset('/uploads/service_category/thumbnails/'.$service_category->thumbnail_image) }}"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right float-end">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.category.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>

        </div>

    </div>
@endsection


@push('js')
    <script>
        $('#name').focusout(function () {

            var name = $(this).val();
            name = name.replace(/\s+/g, '-').toLowerCase();

            $('#slug').val(name);
        })
    </script>
@endpush


