<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Role;


use App\Models\User;
use App\Models\UserRole;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Events\SendReferralCodeWithPhone;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users = User::where('user_type', 'vendor')->orderBy('id', 'DESC')->get();

        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        $cities = [
            'Accra', 'Kumasi', 'Tamale', 'Sekondi', 'Obuase', 'Tema', 'Cape Coast', 'Koforidua', 'Ho', 'Wa', 'Bawku', 'Sunyani', 'Bolgatanga', 'Aflao', 'Nkawkaw', 'Hohoe', 'Winneba', 'Berekum', 'Techiman', 'Sefwi Wiawso', 'Goaso', 'Dambai', 'Nalerigu', 'Damongo'
        ];
        $roles = Role::where('id', 3)->get();
        $categories = Category::all();
        return view('backend.users.create', compact('roles', 'categories', 'cities'));
    }

    public function store(Request $request)
    {

        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required|unique:users,phone_number',
            'short_description' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'role_id.required' => 'Role is required.',
            'category_id.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'short_description.required' => 'Short description  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $name_with_time = time() . '_' . $name;
            $image->move($destinationPath, $name_with_time);
            $profile_image = $name_with_time;
        } else {

            $profile_image = 'default.png';
        }
        if ($request->has('image_one')) {
            $image = $request->file('image_one');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_one = $finalName;
        } else {
            $image_one = NULL;
        }
        if ($request->has('image_two')) {
            $image = $request->file('image_two');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_two = $finalName;
        } else {
            $image_two = NULL;
        }
        if ($request->has('image_three')) {
            $image = $request->file('image_three');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_three = $finalName;
        } else {
            $image_three = NULL;
        }
        if ($request->has('image_four')) {
            $image = $request->file('image_four');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_four = $finalName;
        } else {
            $image_four = NULL;
        }


        $role = Role::find((int)$request->get('role_id'));


        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'city' => $request->get('city'),
            'role_id' => $request->get('role_id'),
            'category_id' => $request->get('category_id'),
            'phone_number' => $request->get('phone_number'),
            'short_description' => $request->get('short_description'),
            'address' => $request->get('address'),
            'referral_code' => $random_string,
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'profile_pic' => $profile_image,
            'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
            'subscription_status' => $request->get('subscription_status'),
            'image_one' => $image_one,
            'image_two' => $image_two,
            'image_three' => $image_three,
            'image_four' => $image_four,
        ]);


        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $phone_number = $user->phone_number;


        $message = 'Thank you for becoming a member of Saver Buddy! Someone from our team will be calling you shortly to complete your registration process. Thank you!';
//        event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));


        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor created successfully.'
            ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::where('id', 3)->get();
        $categories = Category::all();
        $cities = [
            'Accra', 'Kumasi', 'Tamale', 'Sekondi', 'Obuase', 'Tema', 'Cape Coast', 'Koforidua', 'Ho', 'Wa', 'Bawku', 'Sunyani', 'Bolgatanga', 'Aflao', 'Nkawkaw', 'Hohoe', 'Winneba', 'Berekum', 'Techiman', 'Sefwi Wiawso', 'Goaso', 'Dambai', 'Nalerigu', 'Damongo'
        ];
        return view('backend.users.edit', compact('user', 'roles', 'categories', 'cities'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required',
            'short_description' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'category_id.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'short_description.required' => 'Short description is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);

        $user = User::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $name_with_time = time() . '_' . $name;
            $image->move($destinationPath, $name_with_time);
            $profile_image = $name_with_time;
        } else {
            $profile_image = $user->profile_pic;
        }


        if ($request->has('image_one')) {
            $image = $request->file('image_one');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_one = $finalName;
        } else {
            $image_one = $user->image_one;
        }
        if ($request->has('image_two')) {
            $image = $request->file('image_two');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_two = $finalName;
        } else {
            $image_two = $user->image_two;
        }
        if ($request->has('image_three')) {
            $image = $request->file('image_three');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_three = $finalName;
        } else {
            $image_three = $user->image_three;
        }
        if ($request->has('image_four')) {
            $image = $request->file('image_four');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $finalName = rand() . $name;
            $image->move($destinationPath, $finalName);
            $image_four = $finalName;
        } else {
            $image_four = $user->image_four;
        }

        $phone_number = $user->phone_number;
        $role = Role::find((int)$request->get('role_id'));


        if (isset($request->password) && !is_null($request->password)) {
            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'city' => $request->get('city'),
                'role_id' => $request->get('role_id'),
                'category_id' => $request->get('category_id'),
                'phone_number' => $request->get('phone_number'),
                'short_description' => $request->get('short_description'),
                'address' => $request->get('address'),
                'user_type' => $role->name,
                'status' => $request->get('status'),
                'subscription_status' => $request->get('subscription_status'),
                'email' => $request->get('email'),
                'profile_pic' => $profile_image,
                'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
                'password' => bcrypt($request->get('password')),
                'image_one' => $image_one,
                'image_two' => $image_two,
                'image_three' => $image_three,
                'image_four' => $image_four,
            ]);
        } else {
            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'city' => $request->get('city'),
                'role_id' => $request->get('role_id'),
                'category_id' => $request->get('category_id'),
                'phone_number' => $request->get('phone_number'),
                'short_description' => $request->get('short_description'),
                'address' => $request->get('address'),
                'user_type' => $role->name,
                'status' => $request->get('status'),
                'subscription_status' => $request->get('subscription_status'),
                'email' => $request->get('email'),
                'profile_pic' => $profile_image,
                'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
                'image_one' => $image_one,
                'image_two' => $image_two,
                'image_three' => $image_three,
                'image_four' => $image_four,
            ]);
        }


        (new \App\Models\UserRole)->update([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $message = 'We are pleased to confirm your membership with Saver Buddy. You can book an appointment anytime using our app, website, phone or WhatsApp. We look forward to providing you our luxury home spa services.';
        //  event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor has been deleted'
            ]);
    }


    public function updateLocation(Request $request)
    {
        $user = User::findOrFail($request->user_id);

        $user->update([
            'current_address' => $request->current_address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude
        ]);

        return redirect()->route('admin.users.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor Location Updated Successfully.'
            ]);
    }
}
