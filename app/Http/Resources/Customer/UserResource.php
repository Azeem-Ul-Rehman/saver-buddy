<?php

namespace App\Http\Resources\Customer;

use App\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ((!is_null($this->profile_pic))) {
            $profile_pic = $this->profile_pic;
        } else {
            $profile_pic = 'default.png';
        }
        return [
            "id" => $this->id,
            "name" => $this->first_name . " " . $this->last_name,
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "date_of_birth" => $this->date_of_birth,
            "nationality" => $this->nationality,
            "occupation" => $this->occupation,
            "institution_name" => $this->institution_name,
            "address" => $this->address,
            "address_two" => $this->address_two,
            "region_capital" => $this->region_capital,
            "country" => $this->country,
            "email" => $this->email,
            "phone_number" => $this->phone_number,
            "user_type" => $this->user_type,
            "status" => $this->status,
            "subscription_status" => $this->subscription_status,
            "profile_pic" => asset("/uploads/user_profiles/" . $profile_pic),
            "new_promotions" => $this->new_promotions === 1 ? true : false,
            "update_promotions" => $this->update_promotions === 1 ? true : false,
            "new_orders" => $this->new_orders === 1 ? true : false,


        ];
    }
}
