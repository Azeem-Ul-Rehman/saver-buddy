<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Http\Controllers\Api\Customer\QueryException;
use App\Http\Controllers\Controller;
use App\Http\Resources\Customer\UserResource;
use App\Models\User;
use App\Traits\GeneralHelperTrait;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Mail;


class UserVerificationController extends Controller
{
    use GeneralHelperTrait;

    public $successStatus = 200;

    public function verifyUser(Request $request)
    {
        $details = ['phone_number' => $request->phone_number];

        $validator = Validator::make($details, [
            'phone_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()->first()], 200);
        }

        try {
            $user = User::where([
                'phone_number' => $request->phone_number,
                'user_type' => 'customer'
            ])->first();

            if (!is_null($user)) {
                $message = 'User found';
                $data = new UserResource($user);
            } else {
                $message = 'User not found';
                $data = [];
            }
            return response()->json(['success' => true, 'message' => $message, 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 200);
        }

    }

    public function getUser()
    {
        $user = new UserResource(Auth::user());
        return response()->json(['success' => true, 'message' => '', 'data' => $user], 200);
    }


    public function sendOtpCode(Request $request)
    {
        $details = ['phone_number' => $request->phone_number];

        $validator = Validator::make($details, [
            'phone_number' => 'required|exists:users,phone_number',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors(), 'is_otp_send' => false], 200);
        }

        try {
            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'user_type' => 'customer'
            ])->first();

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 6);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);

            $phone_number = $request->phone_number;
            $message = 'Your OTP Code is ' . $otp_code . ' Please enter this code to verify your mobile number.';
//            event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
            $smsResponse = $this->sendSMS($phone_number, $message);

            $data = [
                'phone_number' => $user->phone_number,
                'code' => $otp_code,
            ];

            return response()->json(['success' => true, 'message' => $message, 'data' => $data, 'is_otp_send' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function verifyOtpCode(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|exists:users,phone_number',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        try {
            $user = User::where([
                'phone_number' => $request->get('phone_number'),
                'otp_code' => $request->get('otp_code'),
                'user_type' => 'customer'
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->status = 'verified';
                $user->email_verified_at = now();
                $user->otp_code = null;
                $user->save();

                return response()->json(['success' => true, 'message' => 'OTP Code verified successfully.', 'data' => []], 200);
            } else {
                return response()->json(['success' => false, 'message' => 'Provided OTP code is not correct.', 'data' => []], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

}
