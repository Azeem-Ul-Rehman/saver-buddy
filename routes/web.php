<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if(auth()->check()){
        $user = auth()->user();
        if ($user->hasRole('admin')) {
            return redirect()->route('admin.dashboard.index');
        }
        Auth::logout();

        return redirect()->route('login')->with([
            'flash_status' => 'error',
            'flash_message' => 'You are Unauthorized for this login.'
        ]);
    }
    return view('auth.login');
})->name('index');


Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
//    \Illuminate\Support\Facades\Artisan::call('storage:link');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');

//    \Illuminate\Support\Facades\Artisan::call('passport:install');
//    \Illuminate\Support\Facades\Artisan::call('migrate');

    return 'Commands run successfully Cleared.';
});


// Authentication Routes...
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register/{referral_code?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::post('otp', 'Auth\ForgotPasswordController@otp')->name('otp.send');
Route::post('verify/otp', 'Auth\ForgotPasswordController@verifyOTP')->name('verify.otp');
Route::post('forgot-password-request', 'Auth\ForgotPasswordController@ForgotPasswordRequest')->name('otp.password.reset');

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/meta-tags', 'Backend\MetaTagController', [
            'only' => ['index', 'create', 'store', 'edit', 'update']
        ]);

        Route::resource('/customers', 'Backend\CustomerController');
        Route::resource('/users', 'Backend\UserController');
        Route::resource('/promotions', 'Backend\PromotionController');
        Route::resource('/orders', 'Backend\OrderController');
        Route::post('/users/location', 'Backend\UserController@updateLocation')->name('user.location');
        Route::resource('/category', 'Backend\CategoryController');
        Route::resource('/sliders', 'Backend\SliderController');
        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'edit', 'update']
        ]);
        Route::resource('/contacts', 'Backend\ContactUsController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('/aboutus', 'Backend\AboutUsController', [
            'only' => ['index', 'store']
        ]);

        Route::resource('/privacypolicy', 'Backend\PrivacyController', [
            'only' => ['index', 'store']
        ]);


        //User Profiles
        Route::get('profile', 'Backend\ProfileController@index')->name('profile.index');

        Route::get('edit/profile', 'Backend\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Backend\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Backend\ProfileController@updateProfile')->name('update.user.profile');

        Route::get('update/password/', 'Backend\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Backend\ProfileController@updatePassword')->name('update.user.password');

        Route::get('update/phone', 'Backend\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Backend\ProfileController@updateMobileNumber')->name('update.user.phone');


    });


});


//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('update/promotion-status', 'Backend\AjaxController@updatePromotionStatus')->name('update.promotion-status');
    Route::get('update/promotion-is-featured', 'Backend\AjaxController@updatePromotionIsFeatured')->name('update.promotion-is-featured');
    Route::get('update/customer-subscription-status', 'Backend\AjaxController@updateCustomerSubscriptionStatus')->name('update.customer-subscription-status');


});

