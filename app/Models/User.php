<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'city', 'area_id', 'role_id', 'category_id', 'dob', 'age', 'phone_number', 'emergency_number', 'cnic', 'address', 'referral_code', 'user_type', 'status', 'profile_pic', 'otp_code', 'current_address', 'latitude', 'longitude',
        'date_of_birth', 'nationality', 'occupation', 'institution_name', 'address_two', 'region_capital', 'is_featured', 'subscription_status', 'country', 'short_description', 'fcm_token', 'new_promotions', 'update_promotions', 'new_orders',
        'image_one', 'image_two', 'image_three', 'image_four'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role')->withTimestamps();
    }


    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'This action is unauthorized.');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function referral_code()
    {
        return $this->referral_code;
    }



//     $this->middleware('role:SUPER_ADMIN');
//      $this->middleware('role:STAFF');
//      $this->middleware('role:CUSTOMER');
//      $this->middleware('role:DRIVER');

}
