<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use SoapClient;

trait GeneralHelperTrait
{
    public function orderNumber(): string
    {

        $order_number = DB::table('order_numbers')->first();
        if (!empty($order_number) && !is_null($order_number)) {
            if ($order_number->code == '9999') {

                $last_ch = ord($order_number->alpha);
                $last_ch = $last_ch + 1;
                $new_character = chr($last_ch);

                if (($last_ch >= 65 && $last_ch <= 90) || ($last_ch >= 97 && $last_ch <= 122)) {
                    DB::table('order_numbers')
                        ->insert([
                            'slug' => 'Q-',
                            'alpha' => $new_character,
                            'code' => '0001'
                        ]);
                }

                $new_order_number = 'Q-' . $new_character . '0001';
            } else {
                $existing_order_number = DB::table('order_numbers')
                    ->where('alpha', $order_number->alpha)
                    ->first();

                if ($existing_order_number) {
                    $new_order_number = $existing_order_number->slug . $existing_order_number->alpha . $existing_order_number->code;
                    DB::table('order_numbers')
                        ->update([
                            'slug' => 'Q-',
                            'alpha' => $order_number->alpha,
                            'code' => $this->code($existing_order_number->code)
                        ]);

                } else {
                    DB::table('order_numbers')
                        ->insert([
                            'slug' => 'Q-',
                            'alpha' => $order_number->alpha,
                            'code' => '0001'
                        ]);
                    $new_order_number = 'Q-' . $order_number->alpha . '0001';
                }
            }
        } else {
            DB::table('order_numbers')
                ->insert([
                    'slug' => 'Q-',
                    'alpha' => 'A',
                    'code' => '0001'
                ]);
            $new_order_number = 'Q-' . 'A' . '0001';
        }


        return $new_order_number;
    }

    public function code(string $code)
    {
        $convert_into_int = (int)$code;
        $incremented_int = $convert_into_int + 1;
        $integer_length = (int)log10($incremented_int) + 1;

        if ($integer_length === 1) {
            $new_code = '000' . $incremented_int;
        } elseif ($integer_length === 2) {
            $new_code = '00' . $incremented_int;
        } elseif ($integer_length === 3) {
            $new_code = '0' . $incremented_int;
        } else {
            $new_code = $incremented_int;
        }

        return $new_code;
    }

    public function sendSMS($phone_no, $text)
    {
        try {
//
//
//            $query = array(
//                "clientid" => "horgwiqa",
//                "clientsecret" => "qfptsptn",
//                "from" => "Saverbuddy",
//                "to" => $phone_no,
//                "content" => $text
//            );
//
//            $curl = curl_init();
//
//            curl_setopt_array($curl, [
//                CURLOPT_URL => "https://devp-sms03726-api.hubtel.com/v1/messages/send?" . http_build_query($query),
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_CUSTOMREQUEST => "GET",
//                CURLOPT_SSL_VERIFYHOST => 0,
//                CURLOPT_SSL_VERIFYPEER => 0
//            ]);
//
//            $response = curl_exec($curl);
//            $error = curl_error($curl);
//
//            curl_close($curl);
//
//            if ($error) {
//                return "cURL Error #:" . $error;
//            } else {
//                return \GuzzleHttp\json_decode($response);
//            }
////
            $clientSecret = 'bbizwesm';
            $clientId = 'zvugwogg';
            $mask = 'SAVERBUDDY';
            $to = $phone_no;
            $message = $text;


            $url = "https://sms.hubtel.com/v1/messages/send?clientsecret=" . $clientSecret . "&clientid=" . $clientId . "&from=" . $mask . "&to=" . $to . "&content=" . urlencode($message);
            //setting the curl parameters.
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


            $data = curl_exec($ch);

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


            if (!curl_errno($ch)) {

                $info = curl_getinfo($ch);

            }

            curl_close($ch);
            return $data;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function sendPushNotification($to = '', $data = array())
    {

        $api_key = "AAAAW6ShIcM:APA91bGaZqLqpjnntywkiUpsT7VqIUO_T3lAeD0iizHcD9QxXZ1a0MnQ-daU2K5DxRNtAGLXN-OR2-WM6HGXs_qJn_gQ7_VPr7zCHlgHoNDf-1fACKuPkKjPV_AcLS7E8_BcHIFhvMWr"; //FIREBASE KEY
        $msg = array
        (
            'message' => 'here is a message. message',
            'title' => 'This is a title. title',
            'subtitle' => 'This is a subtitle. subtitle',
            'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );

        $fields = array
        (
            'registration_ids' => $to,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        echo $result;


    }

    function send_notification($registatoin_ids, $notification, $device_type)
    {
        $api_key = "AAAAW6ShIcM:APA91bGaZqLqpjnntywkiUpsT7VqIUO_T3lAeD0iizHcD9QxXZ1a0MnQ-daU2K5DxRNtAGLXN-OR2-WM6HGXs_qJn_gQ7_VPr7zCHlgHoNDf-1fACKuPkKjPV_AcLS7E8_BcHIFhvMWr"; //FIREBASE KEY

        $url = 'https://fcm.googleapis.com/fcm/send';
        if ($device_type == "Android") {
            $fields = array(
                'to' => $registatoin_ids,
                'data' => $notification
            );
        } else {
            $fields = array(
                'to' => $registatoin_ids,
                'notification' => $notification
            );
        }
        // Firebase API Key
        $headers = array(
            'Authorization:key='.$api_key,
            'Content-Type:application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    function send_notification_FCM($notification_id, $title, $message, $id,$type) {

        $accesstoken = config('app.fcm')['FCM_SERVER_KEY'];;

        $URL = 'https://fcm.googleapis.com/fcm/send';


        $post_data = '{
            "to" : "' . $notification_id . '",
            "data" : {
              "body" : "",
              "title" : "' . $title . '",
              "type" : "' . $type . '",
              "id" : "' . $id . '",
              "message" : "' . $message . '",
            },
            "notification" : {
                 "body" : "' . $message . '",
                 "title" : "' . $title . '",
                  "type" : "' . $type . '",
                 "id" : "' . $id . '",
                 "message" : "' . $message . '",
                "icon" : "new",
                "sound" : "default"
                },

          }';
        // print_r($post_data);die;

        $crl = curl_init();

        $headr = array();
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: ' . $accesstoken;
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($crl, CURLOPT_URL, $URL);
        curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);

        curl_setopt($crl, CURLOPT_POST, true);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);

        $rest = curl_exec($crl);

        if ($rest === false) {
            // throw new Exception('Curl error: ' . curl_error($crl));
            //print_r('Curl error: ' . curl_error($crl));
            $result_noti = 0;
        } else {

            $result_noti = 1;
        }

        //curl_close($crl);
        //print_r($result_noti);die;
        return $result_noti;
    }
}
