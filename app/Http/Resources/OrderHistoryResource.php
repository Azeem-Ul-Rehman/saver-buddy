<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "customer_id" => $this->customer_id,
            "customer_phone_number" => $this->customer_phone_number,
            "description" => $this->description,
            "order_total" => $this->order_total,
            "discount_percentage" => $this->discount_percentage,
            "discount_price" => $this->discount_price,
            "vat_percentage" => $this->vat_percentage,
            "vat_price" => $this->vat_price,
            "grand_total" => $this->grand_total,
            "order_date" => date('d/m/Y h:i:s A', strtotime($this->created_at))


        ];
    }
}
