<?php

namespace App\Http\Controllers\Backend;

use App\Http\Resources\PromotionResource;
use App\Models\OrderHistory;
use App\Models\Promotion;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $orders = OrderHistory::whereVendorId($request->id)->with('customer')->get();
        return view('backend.orders.index', compact('orders'));
    }

    public function show($id)
    {

    }

    public function create(Request $request)
    {


    }

    public function listing()
    {
    }

    public function findById($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }


}
