@extends('backend.layouts.master')
@section('title','Profile')
@push('css')
    <style>
        .profile-image {
            width: 85% !important;
            height: auto;
        }

        .bannerFields {
            margin-top: 15px;
        }
    </style>
@endpush
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"> Update {{ __('Password') }}</h5>
            <div class="card-body demo-vertical-spacing demo-only-element">
                <div class="col-lg-12">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form action="{{ route('admin.update.user.password') }}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div
                                    class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                                    <label for="current-password">Current
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="current-password"
                                           id="current-password">
                                    @if ($errors->has('current-password'))
                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('current-password') }}</strong>
                                                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div
                                    class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}">
                                    <label for="new-password">New Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password" id="new-password">
                                    @if ($errors->has('new-password'))
                                        <span class="help-block">
                                                                        <strong>{{ $errors->first('new-password') }}</strong>
                                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="new-password-confirm">Re-type
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password-confirm"
                                           id="new-password-confirm">
                                </div>
                            </div>
                        </div>
                        <div class="row text-md-right float-end">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-md-right float-end">
                                <a href="{{ route('admin.dashboard.index') }}"
                                   class="btn btn-info">
                                    Cancel
                                </a>
                                <button type="submit"
                                        class="btn btn-primary">Update
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
@endpush
