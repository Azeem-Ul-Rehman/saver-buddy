<?php

namespace App\Http\Controllers\Backend;

use App\Http\Resources\PromotionResource;
use App\Models\Promotion;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class PromotionController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $promotions = Promotion::whereVendorId($request->id)->with(['category', 'vendor'])->get();
        return view('backend.promotions.index', compact('promotions'));
    }

    public function show($id)
    {

    }

    public function create(Request $request)
    {


    }

    public function listing()
    {
    }

    public function findById($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $promotion = Promotion::findOrFail($id);
        $vendor_id = $promotion->vendor_id;
        $promotion->delete();

        return redirect()->back()
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Promotions has been deleted'
            ]);
    }


}
