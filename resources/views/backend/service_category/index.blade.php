@extends('backend.layouts.master')
@section('title','Categories')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end">
                <a href="{{ route('admin.category.create') }}"
                   class="btn btn-primary float-end">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Category</span>
                                </span>
                </a></h5>
            <div class="table-responsive text-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="text-nowrap">

                        <th> Sr NO.</th>
                        <th> Name</th>
                        <th> Status</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($service_categories))
                        @foreach($service_categories as $service_category)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$service_category->name}}</td>
                                @if($service_category->is_available == 1)

                                    <td><span class="badge bg-label-primary me-1">Active</span></td>
                                @else
                                    <td><span class="badge bg-label-warning me-1">In-active</span></td>
                                @endif

                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                data-bs-toggle="dropdown">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="{{route('admin.category.edit',$service_category->id)}}"
                                               class="dropdown-item"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                                            <form method="post"
                                                  action="{{ route('admin.category.destroy', $service_category->id) }}"
                                                  id="delete_{{ $service_category->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a class="dropdown-item"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$service_category->id?>').submit(); }">
                                                    <i class="bx bx-trash me-1"></i> Delete
                                                </a>
                                            </form>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [2]}
            ],
        });
    </script>
@endpush
