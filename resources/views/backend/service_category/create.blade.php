@extends('backend.layouts.master')
@section('title','Categories')
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"> Add {{ __('Category') }}</h5>
            <div class="card-body demo-vertical-spacing demo-only-element">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.category.store') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first  mb-3">


                                    <div class="form-group row">


                                        <div class="col-md-4">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Name') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   name="name" value="{{ old('name') }}"
                                                   autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label for="slug"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Slug') }}
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="slug" type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug" value="{{ old('slug') }}"
                                                   autocomplete="slug" autofocus>

                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="inputGroupSelect02"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }}</label>

                                            <select id="inputGroupSelect02"
                                                    class="form-select @error('is_available') is-invalid @enderror"
                                                    name="is_available" autocomplete="is_available">
                                                <option value="0">In-active</option>
                                                <option value="1" selected>Active</option>
                                            </select>
                                            @error('is_available')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description" rows="5" cols="15"
                                                      autocomplete="description"></textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="thumbnail_image" id="image_thumb"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('thumbnail_image')}}" type="file"
                                                   id="thumbnail_image"
                                                   class="form-control @error('thumbnail_image') is-invalid @enderror"
                                                   onchange="readURLThumbnail(this)"
                                                   name="thumbnail_image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:none;"
                                                 id="img_thumbnail" src="#"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right float-end">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.category.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>

        </div>

    </div>
@endsection


@push('js')
@endpush


@push('js')
    <script>
        $('#name').focusout(function () {

            var name = $(this).val();
            name = name.replace(/\s+/g, '-').toLowerCase();

            $('#slug').val(name);
        })
    </script>
@endpush
