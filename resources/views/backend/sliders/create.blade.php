@extends('backend.layouts.master')
@section('title','Sliders')
@section('content')

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"> Add {{ __('Slider Image') }}</h5>
            <div class="card-body demo-vertical-spacing demo-only-element">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.sliders.store') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first  mb-3">


                                    <div class="form-group row">
                                        <input type="hidden" name="type" value="image">
                                        <div class="col-md-6">
                                            <label for="promotion_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Promotion') }}</label>

                                            <select id="inputGroupSelect02"
                                                    class="form-select @error('promotion_id') is-invalid @enderror"
                                                    name="promotion_id" autocomplete="promotion_id">
                                                @if(!empty($promotions) && count($promotions) > 0)
                                                    @foreach($promotions as $promotion)
                                                        <option
                                                            value="{{$promotion->id}}" {{ old('promotion_id') === $promotion->id ? 'selected' : '' }}>
                                                            {{ $promotion->company_name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @error('promotion_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="status"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }}</label>

                                            <select id="inputGroupSelect02"
                                                    class="form-select @error('status') is-invalid @enderror"
                                                    name="status" autocomplete="status">
                                                <option
                                                    value="active" {{ old('status') === 'active' ? 'selected' : '' }}>
                                                    Active
                                                </option>
                                                <option
                                                    value="inactive" {{ old('status') === 'inactive' ? 'selected' : '' }}>
                                                    Inactive
                                                </option>
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="thumbnail_image" id="image_thumb"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Image') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('thumbnail_image')}}" type="file"
                                                   id="thumbnail_image"
                                                   class="form-control @error('thumbnail_image') is-invalid @enderror"
                                                   onchange="readURLThumbnail(this)"
                                                   name="thumbnail_image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 style="display:none;"
                                                 id="img_thumbnail" src="#"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right float-end">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.sliders.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>

        </div>

    </div>
@endsection


@push('js')
@endpush


@push('js')
    <script>
        function readURLThumbnail(input) {
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);



                var reader = new FileReader();
                reader.onload = function (e) {

                    if (size > 2000000) {
                        $('#img_thumbnail').attr('src', '');
                        $('#thumbnail_image').val('');
                        toastr.warning("Thumbnail Image Size is exceeding 2 Mb");
                    } else {
                        $('#img_thumbnail').attr('src', e.target.result);
                        $('#img_thumbnail').css("display", "block");
                        $('#hidden-field').val('');
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endpush
