<!DOCTYPE html>

<!-- =========================================================
* Sneat - Bootstrap 5 HTML Admin Template - Pro | v1.0.0
==============================================================

* Product Page: https://themeselection.com/products/sneat-bootstrap-html-admin-template/
* Created by: ThemeSelection
* License: You must have a valid license purchased in order to legally use the theme for your project.
* Copyright ThemeSelection (https://themeselection.com)

=========================================================
 -->
<!-- beautify ignore:start -->
<html
    lang="en"
    class="light-style layout-menu-fixed"
    dir="ltr"
    data-theme="theme-default"
    data-assets-path="../assets/"
    data-template="vertical-menu-template-free"
>
<head>
    <meta charset="utf-8"/>
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>Saver Buddy | @yield('title')</title>

    <meta name="description" content=""/>

    <!-- Favicon -->
    <!-- Favicon -->
    <link rel="icon" href="https://saverbuddy.app/wp-content/uploads/2023/03/cropped-SAVERBUDDY-LOGO_221208_140636-32x32.png" sizes="32x32">
    <link rel="icon" href="https://saverbuddy.app/wp-content/uploads/2023/03/cropped-SAVERBUDDY-LOGO_221208_140636-192x192.png" sizes="192x192">
    <link rel="apple-touch-icon" href="https://saverbuddy.app/wp-content/uploads/2023/03/cropped-SAVERBUDDY-LOGO_221208_140636-180x180.png">
    <meta name="msapplication-TileImage" content="https://saverbuddy.app/wp-content/uploads/2023/03/cropped-SAVERBUDDY-LOGO_221208_140636-270x270.png">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com"/>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/fonts/boxicons.css') }}"/>

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/css/core.css') }}" class="template-customizer-core-css"/>
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/css/theme-default.css') }}"
          class="template-customizer-theme-css"/>
    <link rel="stylesheet" href="{{ asset('backend/assets/css/demo.css') }}"/>

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}"/>

    <link rel="stylesheet" href="{{ asset('backend/assets/vendor/libs/apex-charts/apex-charts.css') }}"/>

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="{{ asset('backend/assets/vendor/js/helpers.js') }}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('backend/assets/js/config.js') }}"></script>

    <!--RTL version:<link href="assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/toastr.css') }}">

    <style>
        .mandatorySign{
            color: red;
        }
    </style>
    @stack('css')
</head>

<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <!-- Menu -->

        @include('backend.includes.sidebar')
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
            <!-- Navbar -->

            @include('backend.includes.navbar')

            <!-- / Navbar -->

            <div class="content-wrapper">
                <!-- Content -->
                <!-- Content wrapper -->
                @yield('content')
                @include('backend.includes.footer')
            </div>

            <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<!-- Core JS -->
<script src="{{ asset('backend/assets/vendor/js/core.js') }}"></script>

<!-- Vendors JS -->
<script src="{{ asset('backend/assets/vendor/libs/apex-charts/apexcharts.js') }}"></script>

<!-- Main JS -->
<script src="{{ asset('backend/assets/js/main.js') }}"></script>

<!-- Page JS -->
<script src="{{ asset('backend/assets/js/dashboards-analytics.js') }}"></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!--end::Global Theme Bundle -->
<script src="{{asset('vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<!--begin::Page Vendors -->
{{--<script src="{{asset('vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>--}}
<!--end::Page Vendors -->
<script src="{{asset('redactor/redactor.min.js')}}"></script>
{{--<script src="{{asset('js/select2.js')}}"></script>--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>

<script type="text/javascript" src="{{ asset('assets/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script>


<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script type="text/javascript" src="{{ asset('backend/js/toastr.min.js') }}">

<script type="text/javascript">

    function readURL(input) {
        console.log(input);
        if (input.files && input.files[0]) {

            let size = input.files[0].size;
            console.log(`Image Size: ${size}`);

            var reader = new FileReader();
            reader.onload = function (e) {



                if (size > 2000000) {
                    $('#img').attr('src', '');
                    $('#image').val('');
                    toastr.warning(" Image Size is exceeding 2 Mb");


                } else{
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display", "block");
                    $('#hidden-field').val('');
                }


            };
            reader.readAsDataURL(input.files[0]);
        }
    }


    function readURLThumbnail(input) {
        if (input.files && input.files[0]) {

            let size = input.files[0].size;
            console.log(`Image Size: ${size}`);



            var reader = new FileReader();
            reader.onload = function (e) {

                if (size > 2000000) {
                    $('#img_thumbnail').attr('src', '');
                    $('#thumbnail_image').val('');
                    toastr.warning("Thumbnail Image Size is exceeding 2 Mb");
                } else {
                    $('#img_thumbnail').attr('src', e.target.result);
                    $('#img_thumbnail').css("display", "block");
                    $('#hidden-field').val('');
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        } else {
            return false;
        }
    }
</script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif
</script>
@stack('models')
@stack('js')
</body>
</html>
