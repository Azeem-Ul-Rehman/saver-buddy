@extends('backend.layouts.master')
@section('title','Sliders')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end">
                <a href="{{ route('admin.sliders.create') }}"
                   class="btn btn-primary float-end">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Slider Image</span>
                                </span>
                </a></h5>
            <div class="table-responsive text-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="text-nowrap">
                        <th>Image</th>
                        <th>Status</th>
                        <th>Promotion</th>
                        <th>Type</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($sliders))
                        @foreach($sliders as $slider)
                            <tr>
                                <td>
                                    @if($slider->type === 'image')
                                        <img width="150" height="150"
                                             class="img-thumbnail"
                                             src="{{ asset('uploads/sliders/'.$slider->image)  }}"
                                             alt="">
                                    @else
                                        <a href="{{$slider->image}}" target="_blank">{{$slider->image}}</a>
                                    @endif
                                </td>
                                @if($slider->status == 'active')
                                    <td><span class="badge bg-label-primary me-1">Active</span></td>
                                @else
                                    <td><span class="badge bg-label-warning me-1">In-active</span></td>
                                @endif
                                <td>{{ $slider->promotion ? $slider->promotion->company_name : '--' }}</td>
                                <td>{{ ucfirst($slider->type) }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                data-bs-toggle="dropdown">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="{{route('admin.sliders.edit',$slider->id)}}"
                                               class="dropdown-item"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                                            <form method="post"
                                                  action="{{ route('admin.sliders.destroy', $slider->id) }}"
                                                  id="delete_{{ $slider->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a class="dropdown-item"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$slider->id?>').submit(); }">
                                                    <i class="bx bx-trash me-1"></i> Delete
                                                </a>
                                            </form>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                {orderable: false, targets: [3]}
            ],
        });
    </script>
@endpush
