<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFullNameUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('date_of_birth')->nullable();
            $table->text('nationality')->nullable();
            $table->text('occupation')->nullable();
            $table->text('institution_name')->nullable();
            $table->text('country')->nullable();
            $table->text('address_two')->nullable();
            $table->text('region_capital')->nullable();
            $table->boolean('is_featured')->default(false);
            $table->enum('subscription_status',['active','inactive'])->default('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('date_of_birth');
            $table->dropColumn('nationality');
            $table->dropColumn('occupation');
            $table->dropColumn('institution_name');
            $table->dropColumn('country');
            $table->dropColumn('address_two');
            $table->dropColumn('region_capital');
            $table->dropColumn('is_featured');
            $table->dropColumn('subscription_status');
        });
    }
}
