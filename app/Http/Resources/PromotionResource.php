<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\FavouritePromotions;
use App\Models\User;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class PromotionResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $category_name = Category::where('id', $this->category_id)->first();
        $favourite = FavouritePromotions::where('customer_id', Auth::id())->where('promotion_id', $this->id)->first();
        $vendor = new UserResource(User::where(['user_type' => 'vendor', 'id' => $this->vendor_id])->first());
        return [
            "id" => $this->id,
            "company_name" => $this->company_name,
            "category_id" => $this->category_id,
            "category_name" => $category_name ? $category_name->name : '',
            "original_price" => $this->original_price,
            "discounted_price" => $this->discounted_price,
            "promotion_details" => $this->promotion_details,
            "promotion_duration" => $this->promotion_duration,
            "status" => $this->status === 'active' ? 'Active' : 'In-active',
            "is_featured" => $this->is_featured === 1 ? true : false,
            "description" => $this->description,
            "city" => $this->city,
            "expiry_date" => date('d-m-Y h:i:s A', strtotime($this->expiry_date)),
            "is_favourite" => is_null($favourite) ? false : true,
            "image" => asset('/uploads/promotions/' . $this->image),
            "vendor" => $vendor
        ];
    }
}
