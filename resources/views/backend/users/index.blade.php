@extends('backend.layouts.master')
@section('title','Vendors')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end">
                <a href="{{ route('admin.users.create') }}"
                   class="btn btn-primary float-end">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Vendor</span>
                                </span>
                </a></h5>
            <div class="table-responsive text-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="text-nowrap">

                        <th> Sr NO.</th>
                        <th> Full Name</th>
                        <th> Email</th>
                        <th> Mobile Number</th>
                        <th> Status</th>
                        <th> Subscription Status</th>
                        <th> Is Featured</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($users))
                        @foreach($users as $user)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ucfirst($user->fullname())}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone_number}}</td>
                                    <td>{{ucfirst($user->status)}}</td>
                                    @if($user->subscription_status == 'active')

                                        <td><span class="badge bg-label-primary me-1">Active</span></td>
                                    @else
                                        <td><span class="badge bg-label-warning me-1">In-active</span></td>
                                    @endif
                                    @if($user->is_featured === 1)
                                        <td><span class="badge bg-label-primary me-1">Yes</span></td>
                                    @else
                                        <td><span class="badge bg-label-warning me-1">No</span></td>
                                    @endif
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                    data-bs-toggle="dropdown">
                                                <i class="bx bx-dots-vertical-rounded"></i>
                                            </button>
                                            <div class="dropdown-menu">

                                                <a href="{{ url('/admin/promotions?id='.$user->id)}}"
                                                   class="dropdown-item"><i class="bx bxs-offer me-1"></i> Promotions</a>

                                                <a href="{{ url('/admin/orders?id='.$user->id)}}"
                                                   class="dropdown-item"><i class="bx bx-cart-alt me-1"></i> Orders</a>
                                                <a href="{{route('admin.users.edit',$user->id)}}"
                                                   class="dropdown-item"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                                                <form method="post"
                                                      action="{{ route('admin.users.destroy', $user->id) }}"
                                                      id="delete_{{ $user->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="dropdown-item"
                                                       href="javascript:void(0)"
                                                       onclick="if(confirmDelete()){ document.getElementById('delete_<?=$user->id?>').submit(); }">
                                                        <i class="bx bx-trash me-1"></i> Delete
                                                    </a>
                                                </form>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "order": [[0, "asc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });
    </script>
@endpush
