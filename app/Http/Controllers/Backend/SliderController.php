<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Promotion;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class SliderController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->user()->hasRole('ADMIN')) {
            $sliders = Slider::orderBy('id', 'DESC')->get();
            return view('backend.sliders.index', compact('sliders'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }


    public function create()
    {


        if (auth()->user()->hasRole('ADMIN') || auth()->user()->hasRole('SUPER_ADMIN')) {
            $promotions = Promotion::whereStatus('active')->get(['id', 'company_name']);
            return view('backend.sliders.create', compact('promotions'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'thumbnail_image' => Rule::requiredIf(function () use ($request) {
                if ($request->get('type') == 'image') {
                    return true;
                }
            }),
            'status' => 'required',
            'promotion_id' => 'required',
        ], [
            'status.required' => 'Status is required.',
            'thumbnail_image.required' => 'Image is required.',
            'promotion_id.required' => 'Promotion is required.',
        ]);
        if ($request->type === 'image') {
            if ($request->has('thumbnail_image')) {
                $image = $request->file('thumbnail_image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/sliders');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $profile_image = $name;
            } else {
                $profile_image = 'default.png';
            }
        }


        $slider = Slider::create([
            'status' => $request->status,
            'image' => $profile_image,
            'type' => $request->type,
            'promotion_id' => $request->promotion_id,

        ]);

        return redirect()->route('admin.sliders.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Slider created successfully.'
            ]);
    }

    public function show($id)
    {
        if (auth()->user()->hasRole('ADMIN')) {
            $slider = Slider::find($id);
            return view('backend.sliders.show', compact('slider'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }

    public function edit($id)
    {
        if (auth()->user()->hasRole('ADMIN')) {
            $slider = Slider::find($id);
            $promotions = Promotion::whereStatus('active')->get(['id', 'company_name']);
            return view('backend.sliders.edit', compact('slider', 'promotions'));
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }


    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'status' => 'required',
            'promotion_id' => 'required',
        ], [
            'status.required' => 'Status is required.',
            'promotion_id.required' => 'Promotion is required.',
        ]);


        $slider = Slider::find($id);

        if ($request->type === 'image') {
            if ($request->has('thumbnail_image')) {
                $image = $request->file('thumbnail_image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/sliders');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $profile_image = $name;
            } else {
                $profile_image = $slider->image;
            }
        }
        $slider->image = $profile_image;
        $slider->status = $request->status;
        $slider->type = $request->type;
        $slider->promotion_id = $request->promotion_id;

        $slider->save();

        return redirect()->route('admin.sliders.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Slider updated Successfully'
            ]);
    }

    public function destroy($id)
    {
        if (auth()->user()->hasRole('ADMIN')) {
            $slider = Slider::findOrFail($id);
            $slider->delete();

            return redirect()->route('admin.sliders.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Slider has been deleted'
                ]);
        } else {
            Auth::logout();

            return redirect()->route('login')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this login.'
            ]);
        }
    }
}
