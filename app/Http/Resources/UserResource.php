<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\FavouriteVendor;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $category_name = Category::where('id', $this->category_id)->first();

        if ((!is_null($this->profile_pic))) {
            $profile_pic = $this->profile_pic;
        } else {
            $profile_pic = 'default.png';
        }
        $favourite = FavouriteVendor::where('customer_id', Auth::id())->where('vendor_id', $this->id)->first();
        return [
            "id" => $this->id,
            "name" => $this->first_name . " " . $this->last_name,
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "email" => $this->email,
            "category_id" => $this->category_id,
            "category_name" => $category_name ? $category_name->name : '',
            "phone_number" => $this->phone_number,
            "address" => $this->address,
            "user_type" => $this->user_type,
            "status" => $this->status,
            "city" => $this->city,
            "subscription_status" => $this->subscription_status,
            "short_description" => $this->short_description,
            "is_favourite" => is_null($favourite) ? false : true,
            "profile_pic" => asset("/uploads/user_profiles/" . $profile_pic),
            "image_one" => !is_null($this->image_one) ? asset("/uploads/user_profiles/" . $this->image_one) : null,
            "image_two" => !is_null($this->image_two) ? asset("/uploads/user_profiles/" . $this->image_two) : null,
            "image_three" => !is_null($this->image_three) ? asset("/uploads/user_profiles/" . $this->image_three) : null,
            "image_four" =>  !is_null($this->image_four) ?asset("/uploads/user_profiles/" . $this->image_four) : null,


        ];
    }
}
