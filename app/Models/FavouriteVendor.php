<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavouriteVendor extends Model
{
    protected $table = "favourite_vendors";
    protected $guarded = [];
}
