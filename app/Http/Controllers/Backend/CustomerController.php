<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Role;


use App\Models\User;
use App\Models\UserRole;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Events\SendReferralCodeWithPhone;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users = User::where('user_type', 'customer')->orderBy('id', 'DESC')->get();

        return view('backend.customers.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        $categories = Category::all();
        return view('backend.customers.create', compact('roles', 'categories'));
    }

    public function store(Request $request)
    {

        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required|unique:users,phone_number',
            'short_description' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'role_id.required' => 'Role is required.',
            'category_id.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'short_description.required' => 'Short description  is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {

            $profile_image = 'default.png';
        }

        $role = Role::find((int)$request->get('role_id'));


        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $request->get('role_id'),
            'category_id' => $request->get('category_id'),
            'phone_number' => $request->get('phone_number'),
            'short_description' => $request->get('short_description'),
            'address' => $request->get('address'),
            'referral_code' => $random_string,
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'profile_pic' => $profile_image,
            'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
            'subscription_status' => $request->get('subscription_status')
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $phone_number = $user->phone_number;


        $message = 'Thank you for becoming a member of Saver Buddy! Someone from our team will be calling you shortly to complete your registration process. Thank you!';
//        event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));


        return redirect()->route('admin.customers.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor created successfully.'
            ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $categories = Category::all();
        return view('backend.customers.edit', compact('user', 'roles', 'categories'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'image' => '|image|mimes:jpg,jpeg,png|max:2048',
            'role_id' => 'required|integer',
            'category_id' => 'required|integer',
            'phone_number' => 'required',
            'short_description' => 'required',
            'address' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255',
        ], [
            'first_name.required' => 'First name  is required.',
            'last_name.required' => 'Last name  is required.',
            'category_id.required' => 'Gender  is required.',
            'phone_number.required' => 'Phone Number  is required.',
            'short_description.required' => 'Short description is required.',
            'status.required' => 'Status  is required.',
            'email.required' => 'Email  is required.',
        ]);

        $user = User::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $user->profile_pic;
        }

        $phone_number = $user->phone_number;
        $role = Role::find((int)$request->get('role_id'));


        $user->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role_id' => $request->get('role_id'),
            'category_id' => $request->get('category_id'),
            'phone_number' => $request->get('phone_number'),
            'short_description' => $request->get('short_description'),
            'address' => $request->get('address'),
            'user_type' => $role->name,
            'status' => $request->get('status'),
            'subscription_status' => $request->get('subscription_status'),
            'email' => $request->get('email'),
            'profile_pic' => $profile_image,
            'is_featured' => $request->get('is_featured') == '1' ? 1 : 0
        ]);
        (new \App\Models\UserRole)->update([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);

        $message = 'We are pleased to confirm your membership with Saver Buddy. You can book an appointment anytime using our app, website, phone or WhatsApp. We look forward to providing you our luxury home spa services.';
        //  event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
        return redirect()->route('admin.customers.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Vendor updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('admin.customers.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Customer has been deleted'
            ]);
    }



}
