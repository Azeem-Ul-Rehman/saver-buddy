<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavouritePromotions extends Model
{
    protected $table = "favourite_promotions";
    protected $guarded = [];
}
