<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="javascript:void(0)" class="app-brand-link">
              <span class="app-brand-logo demo">
              <img alt="" style="margin-left: 60px;" src="{{ asset('frontend/images/dashboard-logo-white2.png')  }}">

              </span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <!-- Dashboard -->
        <li class="menu-item {{ (request()->is('admin/dashboard')) ? 'active' : '' }} ">
            <a href="{{ route('admin.dashboard.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Dashboard</div>
            </a>
        </li>
        <li class="menu-item {{ (request()->is('admin/sliders') || request()->is('admin/sliders/*')) ? 'active' : '' }} ">
            <a href="{{ route('admin.sliders.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-slider"></i>
                <div data-i18n="Analytics">Sliders</div>
            </a>
        </li>
        <li class="menu-item {{ (request()->is('admin/category') || request()->is('admin/category/*')) ? 'active' : '' }} ">
            <a href="{{ route('admin.category.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-category"></i>
                <div data-i18n="Analytics">Categories</div>
            </a>
        </li>
        <li class="menu-item {{ (request()->is('admin/customers') || request()->is('admin/customers/*')) ? 'active' : '' }} ">
            <a href="{{ route('admin.customers.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user"></i>
                <div data-i18n="Analytics">Customers</div>
            </a>
        </li>
        <li class="menu-item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'active' : '' }} ">
            <a href="{{ route('admin.users.index') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-user-plus"></i>
                <div data-i18n="Analytics">Vendors</div>
            </a>
        </li>

    </ul>
</aside>
