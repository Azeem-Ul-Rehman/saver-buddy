<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Promotion;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{

    public function updatePromotionStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'promotion_status' => 'required'
        ], [
            'promotion_status.required' => "Order ID is Required.",
        ]);

        if (!$validator->fails()) {
            $promotion = Promotion::where('id', $request->get('id'))->first();
            $promotion->status = $request->get('promotion_status');
            $promotion->save();


            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }

    public function updatePromotionIsFeatured(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'is_featured' => 'required'
        ], [
            'is_featured.required' => "Order ID is Required.",
        ]);

        if (!$validator->fails()) {
            $promotion = Promotion::where('id', $request->get('id'))->first();
            $promotion->is_featured = $request->is_featured === '1' ? true : false;
            $promotion->save();


            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }

    public function updateCustomerSubscriptionStatus(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'subscription_status' => 'required'
        ], [
            'subscription_status.required' => "Subscription Status is Required.",
        ]);

        if (!$validator->fails()) {
            $customer = User::where('id', $request->get('id'))->first();
            $customer->subscription_status = $request->get('subscription_status');
            $customer->save();


            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }


}
