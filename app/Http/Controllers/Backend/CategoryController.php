<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Category;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $service_categories = Category::orderBy('id', 'DESC')->get();
        return view('backend.service_category.index', compact('service_categories'));
    }

    public function create()
    {
        return view('backend.service_category.create');

    }

    public function show($id)
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'thumbnail_image'                 => 'required|mimes:jpg,jpeg,png|max:2048',
            'slug' => 'required|unique:service_categories,slug',
            'description' => 'required',
        ], [
            'thumbnail_image.required' => 'Image  is required.',
        ]);


        if ($request->has('thumbnail_image')) {
            $image_new = $request->file('thumbnail_image');
            $name_new = $image_new->getClientOriginalName();
            $destinationPathThumbail = public_path('/uploads/service_category/thumbnails');
            $imagePathNre = $destinationPathThumbail . "/" . $name_new;

            $name_with_time = time() . '_' . $name_new;
            $image_new->move($destinationPathThumbail, $name_with_time);
            $thumbnail_image = $name_with_time;

        } else {
            $thumbnail_image = null;
        }

        Category::create([
            'name' => $request->name,
            'slug' => $request->slug,
            'thumbnail_image' => $thumbnail_image,
            'description' => $request->description,
            'is_available' => $request->is_available == '1' ? true : false
        ]);
        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Category created successfully.'
            ]);

    }

    public function edit($id)
    {
        $service_category = Category::find($id);
        return view('backend.service_category.edit', compact('service_category'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'thumbnail_image' => 'mimes:png,jpg,jpeg',
            'description' => 'required',

        ], [
            'thumbnail_image.required' => 'Image  is required.',
        ]);

        $category = Category::find($id);


        if ($request->has('thumbnail_image')) {

            $image_new = $request->file('thumbnail_image');
            $name_new = $image_new->getClientOriginalName();
            $destinationPathThumbail = public_path('/uploads/service_category/thumbnails');
            $imagePathNre = $destinationPathThumbail . "/" . $name_new;

            $name_with_time = time() . '_' . $name_new;
            $image_new->move($destinationPathThumbail, $name_with_time);
            $thumbnail_image = $name_with_time;

        } else {
            $thumbnail_image = $category->thumbnail_image;;
        }
        $category->update([
            'name' => $request->name,
            'slug' => $request->slug,
            'thumbnail_image' => $thumbnail_image,
            'description' => $request->description,
            'is_available' => $request->is_available == '1' ? true : false
        ]);

        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Category updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $service_category = Category::findOrFail($id);
        $service_category->delete();

        return redirect()->route('admin.category.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Category has been deleted'
            ]);
    }
}
