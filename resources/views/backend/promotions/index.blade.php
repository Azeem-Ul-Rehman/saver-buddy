@extends('backend.layouts.master')
@section('title','Promotions')
@push('css')
    <style>
        #m_table_1_wrapper {
            margin: 20px !important;
        }
    </style>
    <style>
        .responsiveTable {
            overflow-x: scroll;
        }

        @media screen and (max-width: 768px) {
            .responsiveTable {
                overflow-x: scroll;
            }
        }

        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

        }

        /*    loading css*/

        .overlay {
            left: 0 !important;
            top: 0 !important;
            width: 100% !important;
            height: 100% !important;
            position: fixed !important;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 99999;
        }

        .overlay__inner {
            left: 0 !important;
            top: 0 !important;
            width: 100% !important;
            height: 100% !important;
            position: absolute !important;
        }

        .overlay__content {
            left: 50% !important;
            position: absolute !important;
            top: 50% !important;
            transform: translate(-50%, -50%) !important;
        }

        .spinner {
            width: 75px !important;
            height: 75px !important;
            display: inline-block !important;
            border-width: 2px !important;
            border-color: rgba(255, 255, 255, 0.05) !important;
            border-top-color: #fff !important;
            animation: spin 1s infinite linear !important;
            border-radius: 100% !important;
            border-style: solid !important;
        }

        @keyframes spin {
            100% {
                transform: rotate(360deg);
            }
        }

    </style>
@endpush
@section('content')
    <div class="overlay" style="display: none">
        <div class="overlay__inner">
            <div class="overlay__content"><span class="spinner"></span></div>
        </div>
    </div>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <h5 class="card-header float-end"><span>Promotions</span></h5>
            <div class="table-responsive val-nowrap">
                <table class="table" id="m_table_1">
                    <thead>
                    <tr class="val-nowrap">

                        <th> Company name</th>
                        <th> Original price</th>
                        <th> Discounted price</th>
                        <th> Promotion details</th>
                        <th> Promotion duration</th>
                        <th> Promotion Status</th>
                        <th> Is Featured</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($promotions))
                        @foreach($promotions as $promotion)
                            <tr>
                                <td> {{ $promotion->company_name }}</td>
                                <td> {{ $promotion->original_price }}</td>
                                <td> {{ $promotion->discounted_price }}</td>
                                <td> {{ $promotion->promotion_details }}</td>
                                <td> {{ $promotion->promotion_duration }}</td>
                                <td>
                                    <select id="inputGroupSelect02"
                                            class="form-select update-promotion-status @error('promotion_status') is-invalid @enderror"
                                            name="promotion_status" autocomplete="promotion_status"
                                            data-promotion-id="{{$promotion->id}}">
                                        <option
                                            value="active" {{ $promotion->status === 'active' ? 'selected' : '' }}>
                                            Active
                                        </option>
                                        <option
                                            value="inactive" {{ $promotion->status === 'inactive' ? 'selected' : '' }}>
                                            Inactive
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <select id="inputGroupSelect02"
                                            class="form-select update-promotion-is-featured @error('is_featured') is-invalid @enderror"
                                            name="is_featured" autocomplete="is_featured"
                                            data-promotion-id="{{$promotion->id}}">
                                        <option
                                            value="1" {{ $promotion->is_featured === 1 ? 'selected' : '' }}>
                                            Yes
                                        </option>
                                        <option
                                            value="0" {{ $promotion->is_featured === 0 ? 'selected' : '' }}>
                                            No
                                        </option>
                                    </select>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                data-bs-toggle="dropdown">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="javascript:void(0)"
                                               data-promotion="{{$promotion}}"
                                               data-expiry-date="{{date('d-m-Y',strtotime($promotion->expiry_date))}}"
                                               data-vendor="{{$promotion->vendor ? $promotion->vendor->fullName() : '--'}}"
                                               data-category="{{$promotion->category ? $promotion->category->name : '--'}}"
                                               data-image="{{ asset('/uploads/promotions/' . $promotion->image) }}"
                                               class="dropdown-item view-detail"><i class="bx bxs-eyedropper me-1"></i>
                                                View Detail</a>

                                            <form method="post"
                                                  action="{{ route('admin.promotions.destroy', $promotion->id) }}"
                                                  id="delete_{{ $promotion->id }}">
                                                @csrf
                                                @method('DELETE')
                                                <a class="dropdown-item"
                                                   href="javascript:void(0)"
                                                   onclick="if(confirmDelete()){ document.getElementById('delete_<?=$promotion->id?>').submit(); }">
                                                    <i class="bx bx-trash me-1"></i> Delete
                                                </a>
                                            </form>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('models')
    <div class="modal fade" id="backDropModal" data-bs-backdrop="static" tabindex="-1">
        <div class="modal-dialog modal-dialog-scrollable">
            <form class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="backDropModalTitle">Promotion Detail</h5>
                    <button
                        type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                    ></button>
                </div>
                <div class="modal-body">

                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="company_name" class="form-label">Company Name</label>
                            <input
                                readonly
                                type="text"
                                id="company_name"
                                class="form-control"
                                placeholder="Enter Name"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="city" class="form-label">City</label>
                            <input
                                readonly
                                type="text"
                                id="city"
                                class="form-control"
                                placeholder="xxxx@xxx.xx"
                            />
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="is_featured" class="form-label">Is Featured</label>
                            <input
                                readonly
                                type="text"
                                id="is_featured"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="status" class="form-label">Status</label>
                            <input
                                readonly
                                type="text"
                                id="status"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>

                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="discounted_price" class="form-label">Discounted Price</label>
                            <input
                                readonly
                                type="text"
                                id="discounted_price"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="original_price" class="form-label">Original Price</label>
                            <input
                                readonly
                                type="text"
                                id="original_price"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="promotion_duration" class="form-label">Promotion duration</label>
                            <input
                                readonly
                                type="text"
                                id="promotion_duration"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="expiry_date" class="form-label">Expiry Date</label>
                            <input
                                readonly
                                type="text"
                                id="expiry_date"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>
                    <div class="row g-2">
                        <div class="col mb-0">
                            <label for="category" class="form-label">Category</label>
                            <input
                                readonly
                                type="text"
                                id="category"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                        <div class="col mb-0">
                            <label for="vendor" class="form-label">Vendor</label>
                            <input
                                readonly
                                type="text"
                                id="vendor"
                                class="form-control"
                                placeholder="DD / MM / YY"
                            />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-3">
                            <label for="promotion_details" class="form-label">Promotion Details</label>
                            <input
                                readonly
                                type="text"
                                id="promotion_details"
                                class="form-control"
                                placeholder="Enter Name"
                            />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea
                                readonly
                                type="text"
                                id="description"
                                class="form-control"
                                placeholder="Enter Name"
                            ></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-3">
                            <img src="" alt="" id="image" style="width: 50%">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                </div>
            </form>
        </div>
    </div>
@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: []}
            ],
        });
        $(document).on('change', '.update-promotion-status', function () {

            form = $(this).closest('form');
            node = $(this);
            var promotion_status = $(this).val();
            var promotion_id = $(this).data('promotion-id');
            var request = {"id": promotion_id, "promotion_status": promotion_status};
            if (promotion_id !== '') {
                $('.overlay').show();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.update.promotion-status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        $('.overlay').hide();
                        if (response.status == "success") {
                            toastr['success']("Promotion Status Updated Successfully");

                        }
                    },
                    error: function () {
                        $('.overlay').hide();
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Order status");
            }

        });
        $(document).on('change', '.update-promotion-is-featured', function () {

            form = $(this).closest('form');
            node = $(this);
            var is_featured = $(this).val();
            var promotion_id = $(this).data('promotion-id');
            var request = {"id": promotion_id, "is_featured": is_featured};
            if (promotion_id !== '') {
                $('.overlay').show();
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.update.promotion-is-featured') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        $('.overlay').hide();
                        if (response.status == "success") {
                            toastr['success']("Record Updated Successfully");

                        }
                    },
                    error: function () {
                        $('.overlay').hide();
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Order status");
            }

        });
        $(document).on('click', '.view-detail', function () {
            var promotion = $(this).data('promotion');
            $('#city').val(promotion.city);
            $('#company_name').val(promotion.company_name);
            $('#description').val(promotion.description);
            $('#discounted_price').val(promotion.discounted_price);
            $('#image').attr('src', $(this).data('image'));
            $('#is_featured').val(promotion.is_featured === 1 ? 'YES' : 'NO');
            $('#original_price').val(promotion.original_price);
            $('#promotion_details').val(promotion.promotion_details);
            $('#promotion_duration').val(promotion.promotion_duration);
            $('#status').val(promotion.status === 'inactive' ? 'In-active' : 'Active');
            $('#expiry_date').val($(this).data('expiry-date'));
            $('#category').val($(this).data('category'));
            $('#vendor').val($(this).data('vendor'));
            $('#backDropModal').modal('show');
        });

    </script>
@endpush
