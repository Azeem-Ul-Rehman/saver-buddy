<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->text('customer_phone_number');
            $table->unsignedBigInteger('vendor_id');
            $table->text('description')->nullable();
            $table->text('order_total')->nullable();
            $table->text('discount_percentage')->nullable();
            $table->text('discount_price')->nullable();
            $table->text('total')->nullable();
            $table->text('vat_percentage')->nullable();
            $table->text('vat_price')->nullable();
            $table->text('grand_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
