<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Http\Resources\PromotionResource;
use App\Jobs\SendQueueNotification;
use App\Models\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class PromotionController extends Controller
{

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'category_id' => 'required',
            'original_price' => 'required',
            'discounted_price' => 'required',
            'promotion_details' => 'required',
            'promotion_duration' => 'required',
            'image' => 'required',
            'is_featured' => 'required',
            'description' => 'required',
            'status' => 'required',
        ], [
            'company_name.required' => 'Company name is required.',
            'category_id.required' => 'Category is required.',
            'original_price.required' => 'Original Price is required.',
            'discounted_price.required' => 'Discounted Price is required.',
            'promotion_details.required' => 'Promotion Details is required.',
            'promotion_duration.required' => 'Promotion duration is required.',
            'image.required' => 'Image is required.',
            'description.required' => 'Description is required.',
            'status.required' => 'Status is required.',
            'is_featured.required' => 'Is featured is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }


        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/promotions');
            $name_with_time = time() . '_' . $name;
            $image->move($destinationPath, $name_with_time);
            $promotion_image = $name_with_time;;
        } else {
            $promotion_image = 'default.png';
        }


        $user = Auth::user();
        $promotion = Promotion::create([
            'company_name' => $request->get('company_name'),
            'category_id' => $request->get('category_id'),
            'vendor_id' => Auth::id(),
            'original_price' => $request->get('original_price'),
            'discounted_price' => $request->get('discounted_price'),
            'promotion_details' => $request->get('promotion_details'),
            'promotion_duration' => $request->get('promotion_duration'),
            'image' => $promotion_image,
            'city' => $user->city,
            'status' => $request->get('status'),
            'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
            'description' => $request->get('description'),
            'expiry_date' => date("Y-m-d H:i:s", strtotime('+' . $request->get('promotion_duration') . ' hours'))
        ]);

        $details = [
            'title' => 'New Promotion',
            'message' => 'A new promotion has been added by vendor ' . Auth::user()->fullName() . ' named as ' . $request->get('company_name'),
            'variable_name' => 'new_promotions',
            'is_order' => false
        ];

        $job = (new SendQueueNotification($details))->delay(now()->addSeconds(2));
        dispatch($job);

        return response()->json(['success' => true, 'message' => 'Promotion created successfully', 'data' => new PromotionResource($promotion)], 200);
    }

    public function listing()
    {
        $promotions = PromotionResource::collection(Promotion::whereVendorId(Auth::id())->get());
        return response()->json(['success' => true, 'message' => '', 'data' => $promotions], 200);
    }

    public function findById($id)
    {
        $data = Promotion::whereVendorId(Auth::id())->whereId($id)->first();
        if (!is_null($data)) {
            $promotions = new PromotionResource($data);
        } else {
            $promotions = [];
        }

        return response()->json(['success' => true, 'message' => '', 'data' => $promotions], 200);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'category_id' => 'required',
            'original_price' => 'required',
            'discounted_price' => 'required',
            'promotion_details' => 'required',
            'promotion_duration' => 'required',
            'is_featured' => 'required',
            'description' => 'required',
            'status' => 'required',
        ], [
            'company_name.required' => 'Company name is required.',
            'category_id.required' => 'Category is required.',
            'original_price.required' => 'Original Price is required.',
            'discounted_price.required' => 'Discounted Price is required.',
            'promotion_details.required' => 'Promotion Details is required.',
            'promotion_duration.required' => 'Promotion duration is required.',
            'description.required' => 'Description is required.',
            'status.required' => 'Status is required.',
            'is_featured.required' => 'Is featured is required.',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => '', 'data' => [], 'error' => $validator->errors()], 200);
        }

        $promotion = Promotion::whereVendorId(Auth::id())->whereId($id)->first();
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/promotions');
            $name_with_time = time() . '_' . $name;
            $image->move($destinationPath, $name_with_time);
            $promotion_image = $name_with_time;
        } else {
            $promotion_image = $promotion->image;
        }
        $user = Auth::user();
        $promotion->update([
            'company_name' => $request->get('company_name'),
            'category_id' => $request->get('category_id'),
            'vendor_id' => Auth::id(),
            'original_price' => $request->get('original_price'),
            'discounted_price' => $request->get('discounted_price'),
            'promotion_details' => $request->get('promotion_details'),
            'promotion_duration' => $request->get('promotion_duration'),
            'image' => $promotion_image,
            'status' => $request->get('status'),
            'city' => $user->city,
            'is_featured' => $request->get('is_featured') == '1' ? 1 : 0,
            'description' => $request->get('description'),
            'expiry_date' => date("Y-m-d H:i:s", strtotime('+' . $request->get('promotion_duration') . ' hours'))
        ]);


        $details = [
            'title' => 'Update Promotion',
            'message' => 'A promotion has been updated by vendor ' . Auth::user()->fullName() . ' named as ' . $request->get('company_name'),
            'variable_name' => 'update_promotions',
            'is_order' => false
        ];

        $job = (new SendQueueNotification($details))->delay(now()->addSeconds(2));
        dispatch($job);

        return response()->json(['success' => true, 'message' => 'Promotion updated successfully.', 'data' => new PromotionResource($promotion)], 200);
    }

    public function delete($id)
    {
        $data = Promotion::whereVendorId(Auth::id())->whereId($id)->first();
        $data->delete();

        return response()->json(['success' => true, 'message' => 'Promotion deleted successfully', 'data' => []], 200);
    }


}
