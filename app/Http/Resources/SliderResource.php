<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                            => $this->id,
            "type"                          => $this->type,
            'promotion_id'                  => $this->promotion_id,
            'promotion_name'                => $this->promotion ? $this->promotion->company_name : '--' ,
            "image"                         => $this->type === 'image' ? asset("/uploads/sliders/" . $this->image) : NULL
        ];
    }
}
