<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('company_name')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->text('original_price')->nullable();
            $table->text('discounted_price')->nullable();
            $table->text('promotion_details')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->text('promotion_duration')->nullable();
            $table->string('image')->default('default.png');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->boolean('is_featured')->default(0);
            $table->longText('description')->nullable();
            $table->text('city')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
